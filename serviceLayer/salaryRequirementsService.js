var Account = require("../models/accounts").model;
var Message = require("../models/message");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var salaryRequirementsService = function () { };

/**
 * Gets the salaryRequirement levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
salaryRequirementsService.prototype.getSalaryRequirements = function (userId, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = account.salaryRequirements;

            return cb(message);
        }
    });
}

/**
 * Adds the salary requirement for the user.
 * @param userId the user id to update.
 * @param salaryRequirement the salaryRequirement to add.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
salaryRequirementsService.prototype.addSalaryRequirement = function (userId, salaryRequirement, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            salaryRequirement._id = new mongoose.Types.ObjectId;
            account.salaryRequirements.push(salaryRequirement);

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.addSalaryRequirement(userId, salaryRequirement, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Remove the salary requirement for the user.
 * @param userId the user id to update.
 * @param salaryRequirementId Id of the salary requirement to remove (ObjectId).
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
salaryRequirementsService.prototype.removeSalaryRequirement = function (userId, salaryRequirementId, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var index = account.salaryRequirements.findIndex((item) => {
                return item.id === salaryRequirementId;
            });
            if (index >= 0) {
                account.salaryRequirements.splice(index, 1);
            }
            else {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue removing salary requirement.';

                return cb(message);
            }

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.removeSalaryRequirement(userId, salaryRequirementId, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Edit the salary requirement for the user.
 * @param userId the user id to update.
 * @param salaryRequirementId Id of the salary requirement to remove (ObjectId).
 * @param salaryRequirement the salaryRequirement to replace the current object with
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
salaryRequirementsService.prototype.editSalaryRequirement = function (userId, salaryRequirement, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var index = account.salaryRequirements.findIndex((item) => {
                return item.id === salaryRequirement._id;
            });
            account.salaryRequirements[index] = salaryRequirement;

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.editSalaryRequirement(userId, salaryRequirement, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

module.exports = salaryRequirementsService;