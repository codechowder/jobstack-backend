var Company = require("../../models/company").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");

var logoService = function () { };

/**
 * Gets the company logo.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
logoService.prototype.getLogo = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company.logo;

            return cb(message);
        }
    });
}

/**
 * Edit the logo for the company.
 * @param companyId the company id to update.
 * @param logo the company's logo
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
logoService.prototype.editLogo = function (companyId, logo, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {

            company.logo = logo;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = logoService;