var Company = require("../../models/company").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");

var descriptionService = function () { };

/**
 * Gets the company description.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
descriptionService.prototype.getDescription = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company.description;

            return cb(message);
        }
    });
}

/**
 * Edit the description for the company.
 * @param companyId the company id to update.
 * @param description the company's description
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
descriptionService.prototype.editDescription = function (companyId, description, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {

            company.description = description;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = descriptionService;