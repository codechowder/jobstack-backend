var Company = require("../../models/company").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");

var nameService = function () { };

/**
 * Gets the company name.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
nameService.prototype.getName = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company.name;

            return cb(message);
        }
    });
}

/**
 * Edit the name for the company.
 * @param companyId the company id to update.
 * @param name the company's name
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
nameService.prototype.editName = function (companyId, name, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {

            company.name = name;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = nameService;