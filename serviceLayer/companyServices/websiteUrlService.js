var Company = require("../../models/company").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");

var websiteUrlService = function () { };

/**
 * Gets the company websiteUrl.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
websiteUrlService.prototype.getWebsiteUrl = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company.websiteUrl;

            return cb(message);
        }
    });
}

/**
 * Edit the websiteUrl for the company.
 * @param companyId the company id to update.
 * @param websiteUrl the company's websiteUrl
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
websiteUrlService.prototype.editWebsiteUrl = function (companyId, websiteUrl, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {

            company.websiteUrl = websiteUrl;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = websiteUrlService;