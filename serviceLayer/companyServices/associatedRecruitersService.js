var Company = require("../../models/company").model;
var Account = require("../../models/accounts").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");
var mongoose = require('mongoose');
var ObjectId = require('mongoose').Types.ObjectId;

var associatedRecruitersService = function () { };

function GetAccounts(recruiters, cb) {
    Account.find({
        _id: { $in: recruiters }
    }, function (err, accounts) {
        if (err) {
            cb(err, null);
        }
        else {
            cb(null, accounts);
        }
    });
}

/**
 * Gets the recruiter accounts for a company
 * @param companyId the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
associatedRecruitersService.prototype.getAssociatedRecruiters = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        } else {
            var message = new Message();
            var recruiterIds = company.recruiters.map((r) => r.id);

            GetAccounts(recruiterIds, (err, accounts) => {
                if (err) {
                    message.statusCode = 404;
                    message.success = false;
                    message.message = 'Unable to find recruiters.';
                    return cb(message);
                }
                else {
                    var results = [];
                    if (company.recruiters && company.recruiters.length > 0) {
                        var allowedRecruiter = company.recruiters.map((r) => { return r.allow });

                        accounts.forEach((account, index) => {
                            results.push({
                                id: account.id,
                                allow: allowedRecruiter[index],
                                firstName: account.firstName,
                                lastName: account.lastName,
                                email: account.email
                            });
                        });
                        message.statusCode = 200;
                        message.success = true;
                        message.message = results;

                        return cb(message);
                    }
                }
            });
        }
    });
}

/**
 * Adds a recruiter account for a company
 * @param companyId the company id to get.
 * @param accountId the account id to add as a recruiter.
 * @param allowed boolean for if the recruiter is allowed to represent the company
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
associatedRecruitersService.prototype.addAssociatedRecruiter = function (companyId, accountId, allowed, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        } else {


            var recruiter = {
                allow: allowed,
                _id: accountId
            }

            if (company.recruiters.length >= company.subscriptionUserLimit) {
                var message = new Message();
                message.statusCode = 403;
                message.success = false;
                message.message = 'User Limit Reached.';

                return cb(message);
            }

            var index = company.recruiters.findIndex((r) => {
                return r.recruiter.id === accountId;
            });
            if (index >= 0) {
                var message = new Message();
                message.statusCode = 400;
                message.success = false;
                message.message = 'User Already Associated.';

                return cb(message);
            }

            company.recruiters.push(recruiter);

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

/**
 * Adds a recruiter account for a company
 * @param companyId the company id to get.
 * @param accountId the account id to add as a recruiter.
 * @param allowed boolean for if the recruiter is allowed to represent the company
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
associatedRecruitersService.prototype.editAssociatedRecruiter = function (companyId, accountId, allowed, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        } else {

            var index = company.recruiters.findIndex((item) => {
                return item.id === accountId;
            });
            if (index === -1) {
                var message = new Message();
                message.statusCode = 400;
                message.success = false;
                message.message = 'Cannot find Recruiter.';

                return cb(message);
            }

            company.recruiters[index].allow = allowed;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

/**
 * Removes a recruiter account for a company
 * @param companyId the company id to get.
 * @param accountId the account id to remove as a recruiter.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
associatedRecruitersService.prototype.removeAssociatedRecruiter = function (companyId, accountId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        } else {

            var index = company.recruiters.findIndex((item) => {
                return item.id === accountId;
            });
            if (index >= 0) {
                company.recruiters.splice(index, 1);
            }
            else {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue removing recruiter.';
                return cb(message);
            }

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = associatedRecruitersService;