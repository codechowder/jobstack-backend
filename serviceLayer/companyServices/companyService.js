var Company = require("../../models/company").model;
var Account = require("../../models/accounts").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");
var accountFindById = require("../accountFindById");
var AssociatedRecruitersService = require("./associatedRecruitersService");
var ObjectId = require('mongoose').Types.ObjectId;

var associatedRecruitersService = new AssociatedRecruitersService();

var companyService = function () { };

/**
 * Gets the company company.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
companyService.prototype.getCompany = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company;

            return cb(message);

        }
    });
}

companyService.prototype.getUserRequests = function (companyId, userId, cb) {
    Company.findOne({
        "_id": companyId
    }, (err, company) => {
        if (err) {
            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';
            return cb(message);
        }
        else {
            var index = company.recruiters.findIndex((item) => {
                return item.id === userId && item.allow;
            });
            if (index === -1) {
                var message = new Message();
                message.statusCode = 400;
                message.success = false;
                message.message = 'Cannot find Recruiter or cannot access this info.';

                return cb(message);
            }
            else {
                accountFindById(userId, (err, account) => {
                    if (err) {
                        var message = new Message();
                        message.statusCode = 404;
                        message.success = false;
                        message.message = 'Unable to find user.';
                        return cb(message);
                    }
                    else {

                        if (account.roles.indexOf('admin') >= 0 || account.roles.indexOf('company') >= 0) {
                            var recruiterObjectIds = company.recruiters.map((r) => new ObjectId(r.id));
                            var recruiterIds = company.recruiters.map((r) => r.id);
                            Account.find({
                                "allowedViewers._id": {
                                    $in: recruiterObjectIds
                                }
                            }, {
                                    password: 0,
                                    role: 0,
                                    facebookId: 0,
                                    twitterId: 0,
                                    googleId: 0,
                                    linkedInId: 0,
                                    stripeCustomerId: 0,
                                    updated: 0,
                                    subscription: 0
                                }, (err, accounts) => {
                                    if (err) {
                                        var message = new Message();
                                        message.statusCode = 404;
                                        message.success = false;
                                        message.message = 'Unable to find allowed viewers.';
                                        return cb(message);
                                    }
                                    else {
                                        var returnData = [];
                                        var todaysDate = new Date();
                                        accounts.forEach((account) => {

                                            for (var i = 0; i < account.allowedViewers.length; i++) {
                                                var allowedViewer = account.allowedViewers[i];
                                                if (recruiterIds.indexOf(allowedViewer.id) >= 0) {
                                                    var data = {
                                                        account: account,
                                                        expired: allowedViewer.expires.toLocaleDateString()
                                                    };
                                                    if(allowedViewer.expires < todaysDate) {
                                                        //Remove the alloacedViewer
                                                        account.allowedViewers.splice(i, 1);
                                                        account.save();
                                                        i--;
                                                    }
                                                    else 
                                                    {
                                                        if (!allowedViewer.allowView) {
                                                            data.account.firstName = "";
                                                            data.account.lastName = "";
                                                            data.account.email = "";
                                                        }
                                                        returnData.push(data);
                                                    }
                                                }
                                            }
                                        });
                                        var message = new Message();
                                        message.statusCode = 200;
                                        message.success = true;
                                        message.message = returnData;

                                        return cb(message);
                                    }
                                });
                        }
                        else {
                            Account.find({
                                "allowedViewers._id": new ObjectId(userId)
                            }, {
                                    password: 0,
                                    role: 0,
                                    facebookId: 0,
                                    twitterId: 0,
                                    googleId: 0,
                                    linkedInId: 0,
                                    stripeCustomerId: 0,
                                    updated: 0,
                                    subscription: 0
                                }, (err, accounts) => {
                                    if (err) {
                                        var message = new Message();
                                        message.statusCode = 404;
                                        message.success = false;
                                        message.message = 'Unable to find allowed viewers.';
                                        return cb(message);
                                    }
                                    else {
                                        var returnData = [];
                                        var todaysDate = new Date();
                                        accounts.forEach((account) => {
                                            for (var i = 0; i < account.allowedViewers.length; i++) {
                                                var allowedViewer = account.allowedViewers[i];
                                                if (allowedViewer.id === userId) {
                                                    var data = {
                                                        account: account,
                                                        expired: allowedViewer.expires.toLocaleDateString()
                                                    };
                                                    if(allowedViewer.expires < todaysDate) {
                                                        //Remove the alloacedViewer
                                                        account.allowedViewers.splice(i, 1);
                                                        account.save();
                                                        i--;
                                                    }
                                                    else 
                                                    {
                                                        if (!allowedViewer.allowView) {
                                                            data.account.firstName = "";
                                                            data.account.lastName = "";
                                                            data.account.email = "";
                                                        }
                                                        returnData.push(data);
                                                    }
                                                }
                                            }
                                        });
                                        var message = new Message();
                                        message.statusCode = 200;
                                        message.success = true;
                                        message.message = returnData;

                                        return cb(message);
                                    }
                                });
                        }
                    }
                });
            }
        }
    });
}

/**
 * Gets the asscoiated companies for a user.
 * @param userId the user id to get the companies from.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
companyService.prototype.getCompanies = function (userId, cb) {
    Company.find({
        "recruiters._id": userId
    }, (err, companies) => {
        if (err) {
            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = companies;

            return cb(message);
        }
    });
}

/**
 * Gets the asscoiated companies for a user.
 * @param userId the user id to get the companies from.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
companyService.prototype.getAllCompanies = function (cb) {
    Company.find({
    }, { 
        name: 1,
        _id: 1
    }, (err, companyNames) => {
        if (err) {
            var message = new Message();
            message.statusCode = 500;
            message.success = false;
            message.message = 'Error fetching Names';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = companyNames;

            return cb(message);
        }
    });
}

/**
 * Add a company.
 * @param company the company
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
companyService.prototype.addCompany = function (company, userId, cb) {
    company._id = null;

    var newCompany = new Company(company);

    Account.findOne({
        _id: userId
    }, (err, account) => {
        if(account.roles.indexOf('company') === -1) {
            account.roles.push('company');
            account.save();
        }
    });

    newCompany.recruiters.push({
        allow: true,
        _id: userId
    });

    // Attempt to save the user
    newCompany.save(function (err, company) {
        if (err) {
            var message = new Message();
            message.statusCode = 500;
            message.success = false;
            message.message = 'Unable to create company.';
            return cb(message);
        } else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company;

            return cb(message);
        }
    });
}

/**
 * Removes a company.
 * @param company the company id to remove.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
companyService.prototype.removeCompany = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }).remove(function (err) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;

            return cb(message);
        }
    });
}

module.exports = companyService;