var Company = require("../../models/company").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");

var subscriptionUserLimitService = function () { };

/**
 * Gets the company subscriptionUserLimit.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
subscriptionUserLimitService.prototype.getSubscriptionUserLimit = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company.subscriptionUserLimit;

            return cb(message);
        }
    });
}

/**
 * Edit the subscriptionUserLimit for the company.
 * @param companyId the company id to update.
 * @param subscriptionUserLimit the company's subscriptionUserLimit
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
subscriptionUserLimitService.prototype.editSubscriptionUserLimit = function (companyId, subscriptionUserLimit, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {

            company.subscriptionUserLimit = subscriptionUserLimit;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = subscriptionUserLimitService;