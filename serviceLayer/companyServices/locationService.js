var Company = require("../../models/company").model;
var Message = require("../../models/message");
var SaveCompanyService = require("./saveCompanyService");

var locationService = function () { };

/**
 * Gets the company location.
 * @param company the company id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
locationService.prototype.getLocation = function (companyId, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = company.location;

            return cb(message);
        }
    });
}

/**
 * Edit the location for the company.
 * @param companyId the company id to update.
 * @param location the company's location
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
locationService.prototype.editLocation = function (companyId, location, cb) {
    Company.findOne({
        _id: companyId
    }, function (err, company) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find company.';

            return cb(message);
        }
        else {

            company.location = location;

            SaveCompanyService(company, (message) => {
                return cb(message);
            });
        }
    });
}

module.exports = locationService;