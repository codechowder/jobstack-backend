var Company = require("../../models/company").model;
var Account = require("../../models/accounts").model;
var Message = require("../../models/message");

var saveCompanyService = function(company, cb){

    company.updated = new Date();

    company.save(function (err) {
        if (err) {

            var message = new Message();
            message.statusCode = 500;
            message.success = false;
            message.message = 'Issue saving.';

            return cb(message);
        }

        var message = new Message();
        message.statusCode = 200;
        message.success = true;
        message.message = 'Successfully saved.';

        return cb(message);

    });
}

module.exports = saveCompanyService;