var Account = require("../../models/accounts").model;
var Company = require("../../models/company").model;
var allowedAccessEnum = require("../../models/allowAccessEnum");

function GetAccountAccess(accountId, adminOnly, roles, cb) {
    Account.findOne({
        _id: accountId
    }, (err, account) => {
        if (err) {
            cb(err, null);
        } else {
            var indexAdmin = account.roles.indexOf('admin');
            if (adminOnly) {
                if (indexAdmin >= 0) {
                    return cb(null, true);
                }
                else {
                    return cb("No Access", null);
                }
            }
            else {
                var foundRole = false;
                for(var i = 0; i < roles.length; i++) {
                    var indexRole = account.roles.indexOf(roles[i]);

                    if (indexRole >= 0 || indexAdmin >= 0) {
                        foundRole = true;
                        return cb(null, true);
                    }
                }
                if (!foundRole) {
                    return cb("No Access", null);
                }
            }
        }
    });
}

/**
 * Checks the user if the data should be accessable or not.
 * @param {allowedAccessEnum} access access allowed
 */
var allowAccess = function (access) {
    return (req, res, next) => {
        if (res.statusCode === 200) {

            var keepSearching = true;

            var currentUserId = req.user.id;
            var companyId = req.params.companyId;

            if (!currentUserId) {
                res.status(401);
                return next(new Error("Unauthorized"));
            }

            if (access === allowedAccessEnum.All) {
                return next();
            }
            else if (access === allowedAccessEnum.UserOnly) {
                GetAccountAccess(currentUserId, false, ["user", "recruiter", "company"], (err, valid) => {
                    if (err) {
                        res.status(401);
                        return next(new Error("Unauthorized"));
                    }
                    else {
                        return next();
                    }
                });
            }
            else if (!companyId) {
                GetAccountAccess(currentUserId, false, ["company", "recruiter"], (err, valid) => {
                    if (err) {
                        res.status(401);
                        return next(new Error("Unauthorized"));
                    }
                    else {
                        return next();
                    }
                });
            }
            else {
                if (access !== allowedAccessEnum.UserOnly) {

                    Company.findOne({
                        _id: companyId
                    }, (err, company) => {
                        if (err || !company) {
                            res.status(401);
                            return next(new Error("Unauthorized"));
                        } else if (access === allowedAccessEnum.CompanyOnly || access === allowedAccessEnum.AdminOnly) {

                            var index = company.recruiters.findIndex((item) => {

                                return item.id === currentUserId;
                            });

                            if (index < 0) {
                                GetAccountAccess(currentUserId, false, ["company"], (err, valid) => {
                                    if (err) {
                                        res.status(401);
                                        return next(new Error("Unauthorized"));
                                    }
                                    else {
                                        return next();
                                    }
                                });
                            }
                            else {
                                GetAccountAccess(currentUserId, access === allowedAccessEnum.AdminOnly, ["company"], (err, valid) => {
                                    if (err) {
                                        res.status(401);
                                        return next(new Error("Unauthorized"));
                                    }
                                    else {
                                        return next();
                                    }
                                });
                            }
                        }
                        else if (access === allowedAccessEnum.Recruiter) {
                            var index = company.recruiters.findIndex((item) => {
                                return item.id === currentUserId;
                            });

                            if (index < 0) {
                                res.status(401);
                                return next(new Error("Unauthorized"));
                            }
                            else {
                                return next();
                            }
                        }
                        else {

                            res.status(401);
                            return next(new Error("Unauthorized"));

                        }

                    });
                }
            }
        }
    };
}

module.exports = allowAccess;