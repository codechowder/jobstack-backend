var Account = require("../models/accounts").model;
var Message = require("../models/message");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

/**
 * Creates a message object with a success of false.
 * @param {*} statusCode 
 * @param {*} message 
 */
function createErrorMsg(statusCode, message) {
    var message = new Message();
    message.statusCode = statusCode;
    message.success = false;
    message.message = message;
    return message;
}

var passwordService = function () { };

/**
 * Changes the user's password.
 * @param userId - the user's id.
 * @param currentPassword - The password the user, current uses to log into the application.
 * @param newPassword - The password the user wants the change the password to.
 * @param repeatPassword - This is the new password but repeat to make sure the user typed the correct password.
 */
passwordService.prototype.changePassword = function (userId, currentPassword, newPassword, repeatPassword, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {
            // Error finding the user.
            var message = createErrorMsg(404, 'Unable to find user.');
            return cb(message);
        } else {
            // Found the user, ok.
            if (currentPassword && newPassword && repeatPassword) {
                // All three parameters are here and the new password ent
                account.comparePassword(currentPassword, (err, isMatch) => {
                    if (isMatch && !err) {
                        // The current password matched the one the user supplied.
                        if (newPassword === repeatPassword) {
                            // The new password and the repeat password matched.
                            account.password = newPassword;
                            account.nonce = new mongoose.Types.ObjectId;

                            account.save((err, account) => {
                                if (err && err.name !== "VersionError") {
                                    var message = new Message();
                                    message.statusCode = 500;
                                    message.success = false;
                                    message.message = 'Issue updating.';

                                    return cb(message);
                                }

                                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                                    //we weren't able to update the doc because someone else modified it first, retry
                                    console.log("Unable to update, retrying ", retries);
                                    //retry with a little delay
                                    setTimeout(() => {
                                        this.changePassword(userId, currentPassword, newPassword, repeatPassword, cb, (retries + 1));
                                    }, 200);
                                } else if (retries >= config.retries) {
                                    //there is probably something wrong, just return an error
                                    var message = new Message();
                                    message.statusCode = 500;
                                    message.success = false;
                                    message.message = 'Issue updating after retries.';

                                    return cb(message);
                                }

                                else {
                                    var message = new Message();
                                    message.statusCode = 200;
                                    message.success = true;
                                    message.message = 'Successfully updated.';

                                    return cb(message);
                                }
                            });
                        } else {
                            // The new password and the repeat password did not match.
                            var message = createErrorMsg(500, 'Changed Password failed. The new passwords did not match.');
                            return cb(message);
                        }
                    } else {
                        // The password the user supplied did not match to current password.
                        var message = createErrorMsg(500, 'Changed Password failed. Password did not match.');
                        return cb(message);
                    }
                });
            } else {
                // All three parameters are not here.
                var message = createErrorMsg(500, 'Unable to change password. One of the required fields is missing.');
                return cb(message);
            }
        }
    });
}

module.exports = passwordService;