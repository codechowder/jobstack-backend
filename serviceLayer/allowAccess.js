var Account = require("../models/accounts").model;
var allowedAccessEnum = require("../models/allowAccessEnum");

/**
 * Checks the user if the data should be accessable or not.
 * @param {allowedAccessEnum} access access allowed
 */
var allowAccess = function (access) {
    return (req, res, next) => {
        if (res.statusCode === 200) {
            var userId = req.params.accountId;
            var currentUserId = req.user.id;
            if (!userId || !currentUserId) {
                res.status(401);
                return next(new Error("Unauthorized"));
            }

            if (req.access === allowedAccessEnum.All) {
                return next();
            }

            if (userId === currentUserId && access <= allowedAccessEnum.UserOnly) {
                return next();
            }

            Account.findOne({
                _id: currentUserId
            }, (err, currentAccount) => {
                if (err || !currentAccount) {
                    res.status(401);
                    return next(new Error("Unauthorized"));
                } else if (currentAccount.roles.indexOf('admin') >= 0) {
                    return next();
                } else if (access === allowedAccessEnum.Recruiter && (currentAccount.roles.indexOf('recruiter') >= 0 ||
                    currentAccount.roles.indexOf('company') >= 0)) {
                    return next();
                }

                else {
                    Account.findOne({
                        _id: userId
                    }, (err, account) => {
                        if (err || !account) {
                            res.status(401);
                            return next(new Error("Unauthorized"));
                        } else {
                            if (req.access === allowedAccessEnum.AdminOnly) {
                                res.status(401);
                                return next(new Error("Unauthorized"));
                            }
                            //TODO:  Is this how this should be used?
                            var index = account.allowedViewers.findIndex((item) => {
                                return item.id === currentUserId && item.allowView;
                            });

                            if (index < 0) {
                                res.status(401);
                                return next(new Error("Unauthorized"));
                            }
                            else {
                                return next();
                            }
                        }
                    });
                }

            });
        }
    };
}

module.exports = allowAccess;