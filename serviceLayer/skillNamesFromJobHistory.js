var Skill = require("../models/skill").model;
var mongoose = require("mongoose");

var skillNamesFromJobHistory = function (accountDto, jobHistory) {
    var skillNames = [];
    if(jobHistory && jobHistory.length > 0) {
        jobHistory.forEach((job, jIndex) => {
            job.skills.forEach((s, sIndex) => {

                var skillIndex = accountDto.skills.findIndex((skill) => {
                    return skill.id === s.toString();
                });

                if (skillIndex >= 0) {

                    jobHistory[jIndex].skills[sIndex] = accountDto.skills[skillIndex].skill;
                }
            });
        });
    }
}

module.exports = skillNamesFromJobHistory;