var Account = require("../models/accounts").model;
var JobHistory = require("../models/jobhistory").model;
var Skill = require("../models/skill");
var Message = require("../models/message");
var SkillsService = require("./skillsService");
var calculateYearsExperience = require("../businessLogic/calculateYearsExperience");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var skillsService = new SkillsService();

function getSkillIds(account, skills) {

    var skillIds = [];
    if (skills && skills.length > 0) {
        skills.forEach((skillName) => {
            var skillIndex = account.skills.findIndex((s) => {
                return s.skill.toLowerCase() === skillName.toLowerCase();
            });
            if (skillIndex >= 0) {
                skillIds.push(account.skills[skillIndex].id)
            }
        });
    }

    return skillIds;
}

var jobHistoryService = function () { };

jobHistoryService.prototype.addSkills = function (userId, skills, cb) {

    if (skills && skills.length > 0) {
        accountFindById(userId, (err, account) => {
            if (err) {
                return cb();
            }
            else {

                var addSkills = [];

                skills.forEach((s) => {
                    var index = -1;
                    index = account.skills.findIndex((current) => {
                        return current.skill === s.skill;
                    });
                    if (index < 0) {
                        addSkills.push(s);
                    }
                });

                if (addSkills.length > 0) {

                    new Promise(
                        (resolve, reject) => {
                            addSkills.forEach((s, index) => {
                                skillsService.addSkill(userId, s, (message, acnt) => {
                                    if (message.statusCode === 200) {
                                    }
                                    if (index === addSkills.length - 1) {
                                        resolve();
                                    }
                                });

                            });
                        }
                    ).then(() => {
                        cb();
                    });
                }
                else {

                    cb();
                }
            }
        });
    }
    else {
        cb();
    }
}

/**
 * Gets the job levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
jobHistoryService.prototype.getJobHistory = function (userId, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = account.jobHistory;

            return cb(message);
        }
    });
}

/**
 * Adds the job history for the user.
 * @param userId the user id to update.
 * @param job the job to add.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
jobHistoryService.prototype.addJobHistory = function (userId, job, skillNames, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            job.skills = getSkillIds(account, skillNames);

            job._id = new mongoose.Types.ObjectId;

            account.jobHistory.push(job);
            account.nonce = new mongoose.Types.ObjectId;

            account.totalYearsExperience = calculateYearsExperience(account, -1);

            if (job.skills && job.skills.length > 0) {
                job.skills.forEach((s) => {
                    var skillExp = calculateYearsExperience(account, s._id);
                    var index = account.skills.findIndex((item) => {
                        return item.id === s._id;
                    });
                    if (index >= 0) {
                        account.skills[index].totalYearsExperience = skillExp;
                    }
                });
            }

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.addJobHistory(userId, job, skillNames, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Remove the job history for the user.
 * @param userId the user id to update.
 * @param jobJistoryId Id of the job history to remove (ObjectId).
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
jobHistoryService.prototype.removeJobHistory = function (userId, jobHistoryId, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var jobHistroyToDelete = null;
            var index = account.jobHistory.findIndex((item) => {
                return item.id === jobHistoryId;
            });
            if (index < 0) {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue removing job history.';
                return cb(message);
            }
            else {

                account.jobHistory.splice(index, 1);
                account.totalYearsExperience = calculateYearsExperience(account, -1);
                account.nonce = new mongoose.Types.ObjectId;

                account.save((err, account) => {
                    if (err && err.name !== "VersionError") {
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating.';

                        return cb(message);
                    }

                    else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                        //we weren't able to update the doc because someone else modified it first, retry
                        console.log("Unable to update, retrying ", retries);
                        //retry with a little delay
                        setTimeout(() => {
                            this.editJobHistory(userId, jobHistoryId, cb, (retries + 1));
                        }, 200);
                    } else if (retries >= config.retries) {
                        //there is probably something wrong, just return an error
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating after retries.';

                        return cb(message);
                    }

                    else {
                        var message = new Message();
                        message.statusCode = 200;
                        message.success = true;
                        message.message = 'Successfully updated.';

                        return cb(message);
                    }
                });
            }
        }
    });
}

/**
 * Edit the job history for the user.
 * @param userId the user id to update.
 * @param jobHistoryId Id of the job history to remove (ObjectId).
 * @param job the job to replace the current object with
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
jobHistoryService.prototype.editJobHistory = function (userId, job, skillNames, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var index = account.jobHistory.findIndex((item) => {
                return item.id === job._id;
            });

            if (index >= 0) {

                job.skills = getSkillIds(account, skillNames);

                account.jobHistory[index] = job;
                account.nonce = new mongoose.Types.ObjectId;

                account.totalYearsExperience = calculateYearsExperience(account, -1);

                if (job.skills && job.skills.length > 0) {
                    job.skills.forEach((s) => {
                        var skillExp = calculateYearsExperience(account, s);
                        var index = account.skills.findIndex((item) => {
                            return item.id === s;
                        });
                        if (index >= 0) {
                            account.skills[index].totalYearsExperience = skillExp;
                        }
                    });
                }

                account.save((err, account) => {
                    if (err && err.name !== "VersionError") {
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating.';

                        return cb(message);
                    }

                    else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                        //we weren't able to update the doc because someone else modified it first, retry
                        console.log("Unable to update, retrying ", retries);
                        //retry with a little delay
                        setTimeout(() => {
                            this.editJobHistory(userId, job, skillNames, cb, (retries + 1));
                        }, 200);
                    } else if (retries >= config.retries) {
                        //there is probably something wrong, just return an error
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating after retries.';

                        return cb(message);
                    }

                    else {
                        var message = new Message();
                        message.statusCode = 200;
                        message.success = true;
                        message.message = 'Successfully updated.';

                        return cb(message);
                    }
                });
            }
            else {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue editing job history.';
                return cb(message);
            }
        }
    });
}

module.exports = jobHistoryService;