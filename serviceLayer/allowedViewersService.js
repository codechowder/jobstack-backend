var Account = require("../models/accounts").model;
var Company = require("../models/company").model;
var Message = require("../models/message");
var mongoose = require("mongoose");
var config = require("../config");

var allowedViewersService = function () { };

/**
 * Gets the job levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
allowedViewersService.prototype.getAllowedViewers = function (userId, cb) {
    Account.findOne({
        _id: userId
    }, function (err, account) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        } else {

            var returnResult = [];
            new Promise(
                (resolve, reject) => {
                    if(!account.allowedViewers || account.allowedViewers.length === 0) {
                        resolve();
                    }
                    account.allowedViewers.forEach((allowedViewer, index) => {
                        Company.findOne({ "recruiters._id": allowedViewer.id }, (err, company) => {
                            if(err) {
                                console.log("issue with finding recruiter");
                            }
                            else if(company) {
                                returnResult.push({
                                    _id: allowedViewer.id,
                                    expires: allowedViewer.expires,
                                    allowView: allowedViewer.allowView,
                                    companyName: company.name,
                                    companyLogo: company.logo
                                });
                            }

                            if (index === account.allowedViewers.length - 1) {
                                resolve();
                            }
                        });
                    });
                }
            ).then(() => {
                var message = new Message();
                message.statusCode = 200;
                message.success = true;
                //TODO:  We may want to have a frontend model for this.
                message.message = returnResult;

                return cb(message);
            });


        }
    });
}

/**
 * Adds an allowed viewer for the user.
 * @param userId the user id to update.
 * @param allowedAccountId the allowed Account's Id to add.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
allowedViewersService.prototype.addAllowedViewer = function (userId, allowedAccountId, cb, retries = 0) {
    Account.findOne({
        _id: userId
    }, function (err, account) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        } else {

            var expiration = new Date();
            expiration.setDate(expiration.getDate() + 14);

            var allowedViewer = {
                expires: expiration,
                _id: allowedAccountId
            }


            var existingId = account.allowedViewers.findIndex((av) => {
                return av._id === allowedAccountId;
            });

            if (existingId >= 0) {
                account.allowedViewer[existingId] = allowedViewer;
            }
            else {

                account.allowedViewers.push(allowedViewer);
            }

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.addAllowedViewer(userId, allowedAccountId, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Remove the allowed Viewer for the user.
 * @param userId the user id to update.
 * @param allowedAccountId Id of the allowed account to remove (ObjectId).
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
allowedViewersService.prototype.removeAllowedViewer = function (userId, allowedAccountId, cb, retries = 0) {
    Account.findOne({
        _id: userId
    }, function (err, account) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        } else {

            var index = account.allowedViewers.findIndex((item) => {
                return item.id === allowedAccountId;
            });
            if (index >= 0) {
                account.allowedViewers.splice(index, 1);
            }
            else {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue removing allowedViewers.';
                return cb(message);
            }

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.removeAllowedViewer(userId, allowedAccountId, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Edit the allowed viewer to see the user or not.
 * @param userId the user id to update.
 * @param allowedViewerId Id of the salary requirement to remove (ObjectId).
 * @param allowView boolean to allow the user to be viewed by the current account or not.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
allowedViewersService.prototype.editAllowedViewer = function (userId, allowedViewerId, allowView, cb, retries = 0) {
    Account.findOne({
        _id: userId
    }, function (err, account) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var index = account.allowedViewers.findIndex((item) => {
                return item.id === allowedViewerId;
            });
            account.allowedViewers[index].allowView = allowView;

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.editAllowedViewer(userId, allowedAccountId, allowView, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

module.exports = allowedViewersService;