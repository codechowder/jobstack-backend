var Account = require("../models/accounts").model;
var Message = require("../models/message");
var config = require("../config");

var accountUpdate = function (userId, account, retries, caller, postObj, cb){
    account.save((err, account) =>{
        if (err && err.name !== "VersionError") {
            var message = new Message();
            message.statusCode = 500;
            message.success = false;
            message.message = 'Issue updating.';

            return cb(message);
        }

        else if ( (err && err.name === "VersionError" || !account) && retries < 10) {
            //we weren't able to update the doc because someone else modified it first, retry
            console.log("Unable to update, retrying ", retries);
            //retry with a little delay
            setTimeout(() => {
                caller(userId, postObj, (retries + 1), cb);
            }, 200);
        } else if (retries >= config.retries) {
            //there is probably something wrong, just return an error
            var message = new Message();
            message.statusCode = 500;
            message.success = false;
            message.message = 'Issue updating after retries.';

            return cb(message);
        }

        else{
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = 'Successfully updated.';

            return cb(message);
        }
    });
}

module.exports = accountUpdate;