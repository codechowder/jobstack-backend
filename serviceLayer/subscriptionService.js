var Account = require("../models/accounts").model;
var Message = require("../models/message");
var StripeLogic = require("../businessLogic/stripeLogic");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var stripeLogic = new StripeLogic();

function editSubscription(userId, subscription, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            account.subscription = subscription;

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        editSubscription(userId, subscription, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Charge the account for becoming premium
 * @param {Account} account account model 
 * @param {String} subscription the subscription (will be premium)
 * @param {function} cb callback function that returns a message type
 */
function chargeAccount(account, subscription, cb) {
    stripeLogic.createCharge(account.stripeCustomerId, account.roles, (err) => {
        if (err) {
            var message = new Message();
            message.statusCode = 500;
            message.success = false;
            message.message = err;

            return cb(message);
        }
        else {
            editSubscription(account._id, subscription, (message) =>{
                return cb(message);
            });

        }
    });
}

var subscriptionService = function () { };

/**
 * Gets the subscription for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
subscriptionService.prototype.getSubscription = function (userId, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = account.subscription;

            return cb(message);
        }
    });
}

/**
 * Edit the subscription for the user.
 * @param userId the user id to update.
 * @param subscription the user's subscription
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
subscriptionService.prototype.editSubscription = function (userId, subscription, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            if (account.subscription !== subscription && subscription === 'premium') {
                //Need to charge for upgrade
                if (account.stripeCustomerId) {
                    chargeAccount(account, subscription, function (message) {
                        return cb(message);
                    });
                }
                else {
                    stripeLogic.createCustomer(account.email, function (err, stripeCustomer) {
                        if (err) {
                            var message = new Message();
                            message.statusCode = 500;
                            message.success = false;
                            message.message = err;
                        }
                        else {
                            account.stripeCustomerId = stripeCustomer.id;
                            chargeAccount(account, subscription, function (message) {
                                return cb(message);
                            });
                        }
                    });
                }
            }

        }
    });
}

module.exports = subscriptionService;