var Account = require("../models/accounts").model;
var Message = require("../models/message");
var config = require("../config");

var degree = require('../models/degrees').model;
var salaryreqs = require('../models/salaryrequirements').model;
var jobhistory = require('../models/jobhistory').model;
var calculateYearsExperience = require("../businessLogic/calculateYearsExperience");

var employerSearchResultsService = function () { };

var ignoreFields = ["ObjectId", "_id", "firstName", "lastName", "password", "role", "facebookId",
    "twitterId", "googleId", "linkedInId", "stripeCustomerId", "updated",
    "allowedViewers", "subscription", "openForJobs", "savedSearches", "previousSearches"];

RegExp.prototype.toJSON = RegExp.prototype.toString;

/**
 * Gets the count of documents given a query
 * @param {any[]} findQuery the query
 * @param {function} cb callback
 */
function GetCount(findQuery, cb) {
    Account.count(findQuery, (err, count) => {
        if (err) {
            return cb(0);
        }
        else {
            return cb(count);
        }
    });
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

employerSearchResultsService.prototype.getQuery = function (searchItems) {

    var andArray = [];

    searchItems.forEach((element) => {
        if (ignoreFields.indexOf(element.key) === -1) {

            var searchItem = {};
            // Gt/Gte/Lt/Lte/Range Operation
            var dotOperator = element.key.indexOf('.');
            if (dotOperator >= 0 && element.key.indexOf('$') >= 0) {
                var $elemMatch = {};
                var leftHand = element.key.substring(0, dotOperator);
                var exp = element.key.substr(dotOperator + 1, element.key.length - (dotOperator + 1));

                dotOperator = exp.indexOf('.');

                if (dotOperator >= 0 && element.value.isArray) {
                    var exp2 = exp.substr(dotOperator + 1, exp.length - (dotOperator + 1));
                    exp = exp.substring(0, dotOperator);
                    $elemMatch[exp] = element.value[0];
                    $elemMatch[exp2] = element.value[1];
                }
                else {
                    //ignore case and a like query
                    var likeIgnoreCase = element.value;
                    if (!Date.parse(likeIgnoreCase) && !Number.isInteger(likeIgnoreCase)) {
                        likeIgnoreCase = { $regex: escapeRegExp(element.value), $options: 'i' };
                    }
                    $elemMatch[exp] = likeIgnoreCase;
                }
                searchItem[leftHand] = $elemMatch;
            }
            else {
                if (!Array.isArray(element.value)) {
                    //ignore case and a like query
                    var likeIgnoreCase = element.value;
                    if (!Date.parse(likeIgnoreCase) && !Number.isInteger(likeIgnoreCase)) {
                        likeIgnoreCase = { $regex: escapeRegExp(element.value), $options: 'i' };
                    }
                    searchItem[element.key] = likeIgnoreCase;
                }
                else {
                    var inArray = [];

                    element.value.forEach((val) => {
                        //ignore case and a like query
                        var likeIgnoreCase = val;
                        if (!Date.parse(likeIgnoreCase) && !Number.isInteger(likeIgnoreCase) && !(likeIgnoreCase === 'true' || likeIgnoreCase === 'false')) {
                            likeIgnoreCase = new RegExp(escapeRegExp(val), "i");
                        }

                        inArray.push(likeIgnoreCase);

                    });
                    searchItem[element.key] = { '$in': inArray };
                }
            }
            andArray.push(searchItem);
        }
    });

    return andArray;
}

employerSearchResultsService.prototype.getResultsFromQuery = function (employerId, findQuery, skip, limit, cb) {
    var count = GetCount(findQuery, (count) => {
        if (count > 0) {

            Account.findById(employerId, (err, account) => {
                var length = account.previousSearches && account.previousSearches.length || 0;
                if (length > config.maxPreviousSearches) {
                    account.previousSearches.shift();
                }
                if (!account.previousSearches) {
                    account.previousSearches = [];
                }
                account.previousSearches.unshift( { query: JSON.stringify(findQuery), date: new Date() });
                account.save();

                Account.find(findQuery, {
                    password: 0,
                    roles: 0,
                    facebookId: 0,
                    twitterId: 0,
                    googleId: 0,
                    linkedInId: 0,
                    stripeCustomerId: 0,
                    updated: 0,
                    subscription: 0
                }).skip(skip).limit(limit).sort({ subscription: -1 }).exec((err, collection) => {
                    if (err) {
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = null;

                        return cb(message);
                    }
                    else {
                        var displayElements = [];
                        collection.forEach((account) => {


                            var index = -1;
                            if (account.allowedViewers) {
                                index = account.allowedViewers.findIndex((item) => {
                                    return item.id === employerId && item.allowView;
                                });
                            }
                            if (index < 0) {
                                account.firstName = "";
                                account.lastName = "";
                                account.email = "";
                                account.profilePic = "";
                            }
                            account.allowedViewers = "";
                            var itemToDisplay = {
                                account: account,
                                currentJobs: account.jobHistory.filter(a => a.isCurrent),
                                totalExp: account.totalYearsExperience ? account.totalYearsExperience.toFixed(2) : 0,
                            };
                            displayElements.push(itemToDisplay);

                        });

                        var message = new Message();
                        message.statusCode = 200;
                        message.success = true;
                        message.message = { displayElements: displayElements, count: count, query: JSON.stringify(findQuery) };

                        return cb(message);
                    }
                });
            });
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = null;

            return cb(message);
        }
    });
}

/**
 * Gets the results of accounts.
 * @param employerId the user id of the requester.
 * @param simpleStr the string query to search items against
 * @param searchItems the key-value search items
 * @param skip number of elements to skip
 * @param limit number of elements to retreive
 * @param isOrQuery is this an or query or an and query? (Default is False)
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
employerSearchResultsService.prototype.getResults = function (employerId, simpleStr, searchItems, skip, limit, isOrQuery, cb) {
    var andQuery = [];
    var orQuery = [];
    var queryObj = [];

    if (simpleStr && simpleStr !== 'undefined' && simpleStr.length > 0) {
        orQuery = this.getQuery(this.getSimpleResults(employerId, simpleStr));
    }

    if (searchItems) {
        queryObj = this.getQuery(searchItems);
    }

    if (isOrQuery) {
        orQuery = queryObj;
    }
    else {
        andQuery = queryObj;
    }

    var findQuery = null;

    if (orQuery.length === 0 && queryObj.length === 0) {
        findQuery = {
            '$and': [
                { roles: "user" },
                { openForJobs: true }
            ]
        };
    }

    else if (andQuery.length > 0 && orQuery.length > 0) {
        findQuery = {
            '$and': [
                { roles: "user" },
                { openForJobs: true },
                { '$and': andQuery },
                { '$or': orQuery }
            ]
        };

    }
    else if (andQuery.length > 0) {
        findQuery = {
            '$and': [
                { roles: "user" },
                { openForJobs: true },
                { '$and': andQuery }
            ]
        };
    }
    else {
        findQuery = {
            '$and': [
                { roles: "user" },
                { openForJobs: true },
                { '$or': orQuery }
            ]
        };

    }

    this.getResultsFromQuery(employerId, findQuery, skip, limit, (message) => {
        return cb(message);
    });
}

/**
 * Using a simple search option, returns the collection of key value pairs.  This result
 * will be more generic and not as useful.  Adding this for UX if it is ever needed
 * @param employerId The user Id requesting the search result
 * @param simpleStr the string query to search items against
 */
employerSearchResultsService.prototype.getSimpleResults = function (employerId, simpleStr) {

    if (simpleStr && simpleStr !== 'undefined') {

        //Construct the search items
        var searchItems = [];

        var accountModel = new Account();

        var schemaArrays = ["salaryRequirements", "degrees", "jobHistory", "skills", "certifications"];

        Object.keys(accountModel._doc).forEach(k => {

            if (ignoreFields.indexOf(k) === -1) {

                var schemaArrayIndex = schemaArrays.indexOf(k);
                if (schemaArrayIndex >= 0) {
                    switch (schemaArrayIndex) {
                        case 0:
                            searchItems.push({
                                key: k + ".location",
                                value: simpleStr
                            });
                            if (Number.isInteger(simpleStr)) {
                                searchItems.push({
                                    key: k + ".salaryRequirement",
                                    value: simpleStr
                                });
                            }
                            break;
                        case 1:
                            searchItems.push({
                                key: k + ".degreeType",
                                value: simpleStr
                            });
                            searchItems.push({
                                key: k + ".school",
                                value: simpleStr
                            });
                            searchItems.push({
                                key: k + ".degree",
                                value: simpleStr
                            });
                            searchItems.push({
                                key: k + ".fieldOfStudy",
                                value: simpleStr
                            });
                            if (Date.parse(simpleStr)) {
                                searchItems.push({
                                    key: k + ".fromDate",
                                    value: simpleStr
                                });

                                searchItems.push({
                                    key: k + ".toDate",
                                    value: simpleStr
                                });
                            }
                            break;
                        case 2:
                            searchItems.push({
                                key: k + ".companyName",
                                value: simpleStr
                            });
                            if (Date.parse(simpleStr)) {
                                searchItems.push({
                                    key: k + ".fromDate",
                                    value: simpleStr
                                });
                                searchItems.push({
                                    key: k + ".toDate",
                                    value: simpleStr
                                });
                            }
                            searchItems.push({
                                key: k + ".description",
                                value: simpleStr
                            });
                            searchItems.push({
                                key: k + ".position",
                                value: simpleStr
                            });

                            break;
                        case 3:
                            searchItems.push({
                                key: k + ".skill",
                                value: simpleStr
                            });
                            if (!isNaN(parseInt(simpleStr))) {
                                searchItems.push({
                                    key: k + ".totalYearsExperience.$gte",
                                    value: simpleStr
                                });
                            }
                            break;
                        case 4:
                            if (Date.parse(simpleStr)) {
                                searchItems.push({
                                    key: k + ".aquired",
                                    value: simpleStr
                                });
                                searchItems.push({
                                    key: k + ".experation",
                                    value: simpleStr
                                });
                            }
                            searchItems.push({
                                key: k + ".name",
                                value: simpleStr
                            });
                            searchItems.push({
                                key: k + ".code",
                                value: simpleStr
                            });
                    }
                }
                else {
                    searchItems.push({
                        key: k,
                        value: simpleStr
                    });
                }


            }
        });
    }

    return searchItems;
}

module.exports = employerSearchResultsService;