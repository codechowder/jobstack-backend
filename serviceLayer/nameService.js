var Account = require("../models/accounts").model;
var Message = require("../models/message");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var nameService = function () { };

/**
 * Gets the job levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
nameService.prototype.getName = function (userId, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = {
                firstName: account.firstName,
                lastName: account.lastName
            };

            return cb(message);
        }
    });
}

/**
 * Edit the name for the user.
 * @param userId the user id to update.
 * @param firstName the user's first name
 * @param lastName the user's last name
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
nameService.prototype.editName = function (userId, firstName, lastName, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            account.firstName = firstName;
            account.lastName = lastName;

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.editName(userId, firstName, lastName, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

module.exports = nameService;