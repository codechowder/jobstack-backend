var Account = require("../models/accounts").model;
var AccountDTO = require("../models/frontendModels/accountDto");
var accountFindById = require("./accountFindById");
var skillNamesFromJobHistory = require("../serviceLayer/skillNamesFromJobHistory");

/**
 * Gets the AccountDto from the database
 * @param {string} userId the user id to search for
 */
var accountDtoService = function(userId) { 

    var promise = new Promise(
        (resolve, reject) => {
            accountFindById(userId, (err, account) =>{
                if(err) {
                    reject("Could not find User!");
                }
                else{
                    var accountDto = new AccountDTO(account, true);
                    skillNamesFromJobHistory(accountDto, accountDto.jobHistory);
                    resolve(accountDto);
                }
            });
        }
    );

    return promise;

}

module.exports = accountDtoService;