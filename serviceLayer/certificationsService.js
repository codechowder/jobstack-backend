var Account = require("../models/accounts").model;
var Message = require("../models/message");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var certificationsService = function () { };

/**
 * Gets the job levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
certificationsService.prototype.getCertifications = function (userId, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = account.certifications;

            return cb(message);
        }
    });
}

/**
 * Adds the certification for the user.
 * @param userId the user id to update.
 * @param certification the certification to add.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
certificationsService.prototype.addCertification = function (userId, certification, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            certification._id = new mongoose.Types.ObjectId;
            account.certifications.push(certification);

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.addCertification(userId, certification, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Remove the certification for the user.
 * @param userId the user id to update.
 * @param certificationId Database Id of the certification to remove.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
certificationsService.prototype.removeCertification = function (userId, certificationId, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var index = account.certifications.findIndex((item) => {
                return item.id === certificationId;
            });
            if (index >= 0) {
                account.certifications.splice(index, 1);
            }
            else {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue removing certification.';
                return cb(message);
            }

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.removeCertification(userId, certificationId, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

/**
 * Edit the certification for the user.
 * @param userId the user id to update.
 * @param certificationId Database Id of the certification to remove.
 * @param certification the certification to replace the current object with
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
certificationsService.prototype.editCertification = function (userId, certification, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var index = account.certifications.findIndex((item) => {
                return item.id === certification._id;
            });
            if (index >= 0) {
                account.certifications[index] = certification;
            }
            else {
                var message = new Message();
                message.statusCode = 500;
                message.success = false;
                message.message = 'Issue editing certification.';
                return cb(message);
            }

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.editCertification(userId, certification, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

module.exports = certificationsService;