var Account = require("../models/accounts").model;
var Message = require("../models/message");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var openForJobsService = function () { };

/**
 * Gets the job levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
openForJobsService.prototype.getOpenForJobs = function (userId, cb) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = account.openForJobs;

            return cb(message);
        }
    });
}

/**
 * Edit the openForJobs for the user.
 * @param userId the user id to update.
 * @param openForJobs is the user open for jobs or not
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
openForJobsService.prototype.editOpenForJobs = function (userId, openForJobs, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            account.openForJobs = openForJobs

            account.nonce = new mongoose.Types.ObjectId;

            account.save((err, account) => {
                if (err && err.name !== "VersionError") {
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating.';

                    return cb(message);
                }

                else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                    //we weren't able to update the doc because someone else modified it first, retry
                    console.log("Unable to update, retrying ", retries);
                    //retry with a little delay
                    setTimeout(() => {
                        this.editOpenForJobs(userId, openForJobs, cb, (retries + 1));
                    }, 200);
                } else if (retries >= config.retries) {
                    //there is probably something wrong, just return an error
                    var message = new Message();
                    message.statusCode = 500;
                    message.success = false;
                    message.message = 'Issue updating after retries.';

                    return cb(message);
                }

                else {
                    var message = new Message();
                    message.statusCode = 200;
                    message.success = true;
                    message.message = 'Successfully updated.';

                    return cb(message);
                }
            });
        }
    });
}

module.exports = openForJobsService;