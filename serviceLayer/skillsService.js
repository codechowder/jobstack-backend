var Account = require("../models/accounts").model;
var Skill = require("../models/skill").model;
var Message = require("../models/message");
var accountFindById = require("./accountFindById");
var mongoose = require("mongoose");
var config = require("../config");

var skillsService = function () { };

/**
 * Gets the job levels for the user.
 * @param userId the user id to get.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
skillsService.prototype.getSkills = function (userId, cb) {
    accountFindById(userId, function (err, account) {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {
            var message = new Message();
            message.statusCode = 200;
            message.success = true;
            message.message = account.skills;

            return cb(message);
        }
    });
}

/**
 * Adds the skill for the user.
 * @param userId the user id to update.
 * @param skill the skill to add.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
skillsService.prototype.addSkill = function (userId, skill, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            var existingSkillIndex = account.skills.findIndex((s) => {
                return s.skill.toLowerCase() === skill.toLowerCase();
            });

            if (existingSkillIndex === -1) {
                var newSkill = new Skill({
                    skill: skill
                });

                account.skills.push(newSkill);

                account.nonce = new mongoose.Types.ObjectId;

                account.save((err, account) => {
                    if (err && err.name !== "VersionError") {
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating.';

                        return cb(message);
                    }

                    else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                        //we weren't able to update the doc because someone else modified it first, retry
                        console.log("Unable to update, retrying ", retries);
                        //retry with a little delay
                        setTimeout(() => {
                            this.addSkill(userId, skill, cb, (retries + 1));
                        }, 200);
                    } else if (retries >= config.retries) {
                        //there is probably something wrong, just return an error
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating after retries.';

                        return cb(message);
                    }

                    else {
                        accountFindById(userId, (err, account) => {
                            if (err) {

                                var message = new Message();
                                message.statusCode = 404;
                                message.success = false;
                                message.message = 'Unable to find user.';

                                return cb(message);
                            }
                            else {
                                var message = new Message();
                                message.statusCode = 200;
                                message.success = true;
                                message.message = 'Successfully added skill.';
                                return cb(message, account);
                            }
                        });
                    }
                });
            }
            else {
                var message = new Message();
                message.statusCode = 200;
                message.success = true;
                message.message = 'Skill already added.';
                return cb(message, account);
            }
        }
    });
}

/**
 * Remove the skills not used for the user.
 * @param userId the user id to update.
 * @param cb the callback function.  @returns message the message to send to the frontend
 */
skillsService.prototype.cleanSkills = function (userId, cb, retries = 0) {
    accountFindById(userId, (err, account) => {
        if (err) {

            var message = new Message();
            message.statusCode = 404;
            message.success = false;
            message.message = 'Unable to find user.';

            return cb(message);
        }
        else {

            //Find all Skill Ids referenced by Job history
            if (account.jobHistory && account.jobHistory.length > 0) {
                var currentSkills = [];
                account.jobHistory.forEach((jobHistory) => {
                    jobHistory.skills.forEach((skill) => {
                        var id = skill.toString();
                        currentSkills.push(id);
                    })
                });

                var deleteSkills = [];
                account.skills.forEach((skill) => {
                    var index = currentSkills.indexOf(skill.id);
                    if (index < 0) {
                        deleteSkills.push(skill.id);
                    }
                });

                for (var i = 0; i < account.skills.length; i++) {
                    var index = deleteSkills.indexOf(account.skills[i].id);
                    if (index >= 0) {
                        account.skills.splice(i, 1);
                        i--;
                    }
                }
                account.save((err, account) => {
                    if (err && err.name !== "VersionError") {
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating.';

                        return cb(message);
                    }

                    else if ((err && err.name === "VersionError" || !account) && retries < 10) {
                        //we weren't able to update the doc because someone else modified it first, retry
                        console.log("Unable to update, retrying ", retries);
                        //retry with a little delay
                        setTimeout(() => {
                            this.cleanSkills(userId, cb, (retries + 1));
                        }, 200);
                    } else if (retries >= config.retries) {
                        //there is probably something wrong, just return an error
                        var message = new Message();
                        message.statusCode = 500;
                        message.success = false;
                        message.message = 'Issue updating after retries.';

                        return cb(message);
                    }

                    else {

                        var message = new Message();
                        message.statusCode = 200;
                        message.success = true;
                        message.message = 'Successfully cleaned skills.';
                        return cb(message);
                    }
                });
            }
        }
    });
}

module.exports = skillsService;