var Account = require("../models/accounts").model;

/**
 * Returns the account based off the id
 * @param {String} id id of the account
 * @param {Function} callback callback function
 */
var accountFindById = function(id, callback) {
    Account.findOne({
        _id: id
    }, (err, account) => {
        if(err){
			console.error("accountFindById:", err);
		} else if(!account){
			var msg = "Could Not accountFindById:" + id;
			console.error(msg);
			return callback(msg);
		}
		callback(err, account);
    });
}

module.exports = accountFindById;