var express = require('express');
var passport = require("passport");
var possibleJobLevels = require('../models/global/possibleJobLevels');
var possibleJobTypes = require('../models/global/possibleJobTypes');
var possibleRoles = require('../models/global/possibleRoles');
var possibleSubscriptionTypes = require('../models/global/possibleSubscriptionTypes');
var allowedAccessEnum = require("../models/allowAccessEnum");
var allowAccess = require("../serviceLayer/allowAccess");

var router = express.Router();

/**
 * Gets all the possible job levels.
 */
router.get('/jobLevels', passport.authenticate('jwt', {
    session: false
}), allowAccess(allowedAccessEnum.All), function (req, res, next) {
    res.status(200).json({
        success: true,
        message: possibleJobLevels
    });
});


/**
 * Gets all the possible job types.
 */
router.get('/jobTypes', passport.authenticate('jwt', {
    session: false
}), allowAccess(allowedAccessEnum.All), function (req, res, next) {
    res.status(200).json({
        success: true,
        message: possibleJobTypes
    });
});

/**
 * Gets all the possible role types.
 */
router.get('/roles', passport.authenticate('jwt', {
    session: false
}), allowAccess(allowedAccessEnum.AdminOnly), function (req, res, next) {
    res.status(200).json({
        success: true,
        message: possibleRoles
    });
});

/**
 * Gets all the possible job types.
 */
router.get('/subscriptionTypes', passport.authenticate('jwt', {
    session: false
}), allowAccess(allowedAccessEnum.All), function (req, res, next) {
    res.status(200).json({
        success: true,
        message: possibleSubscriptionTypes
    });
});

module.exports = router;