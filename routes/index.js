var express = require('express');
var passport = require("passport");

var message = require("../models/message");
var Account = require("../models/accounts").model;
var allowedAccessEnum = require("../models/allowAccessEnum");
var possibleRoles = require("../models/global/possibleRoles");
var possibleJobLevels = require("../models/global/possibleJobLevels");
var possibleJobTypes = require("../models/global/possibleJobTypes");
var possibleSubscriptionTypes = require("../models/global/possibleSubscriptionTypes");

var arrayContainsAll = require("../businessLogic/arrayContainsAll");
var calculateYearsExperience = require("../businessLogic/calculateYearsExperience");

var SaveSearchService = require("../serviceLayer/saveSearchService");
var ProfilePicService = require("../serviceLayer/profilePicService");
var CurrentLivingLocationService = require("../serviceLayer/currentLivingLocationService");
var CountriesEligibilityService = require("../serviceLayer/countriesEligibilityService");
var PortfolioService = require("../serviceLayer/portfolioService");
var SkillsService = require("../serviceLayer/skillsService");
var OpenForJobsService = require("../serviceLayer/openForJobsService");
var HighSchoolDiplomaService = require("../serviceLayer/highSchoolDiplomaService");
var RelocationService = require("../serviceLayer/relocationService");
var CertificationsService = require("../serviceLayer/certificationsService");
var NameService = require("../serviceLayer/nameService");
var EmailService = require("../serviceLayer/emailService");
var RoleService = require("../serviceLayer/roleService");
var SubscriptionService = require("../serviceLayer/subscriptionService");
var JobLevelService = require("../serviceLayer/jobLevelService");
var JobTypeService = require("../serviceLayer/jobTypeService");
var DegreeService = require("../serviceLayer/degreeService");
var PasswordService = require("../serviceLayer/passwordService");
var JobHistoryService = require("../serviceLayer/jobHistroyService");
var SalaryRequirementsService = require("../serviceLayer/salaryRequirementsService");
var AllowedViewersService = require("../serviceLayer/allowedViewersService");
var allowAccess = require("../serviceLayer/allowAccess");
var EmployerSearchResultsService = require("../serviceLayer/employerSearchResultsService");
var accountDtoService = require("../serviceLayer/accountDtoService");

var saveSearchService = new SaveSearchService();
var profilePicService = new ProfilePicService();
var currentLivingLocationService = new CurrentLivingLocationService();
var countriesEligibilityService = new CountriesEligibilityService();
var portfolioService = new PortfolioService();
var skillsService = new SkillsService();
var openForJobsService = new OpenForJobsService();
var highSchoolDiplomaService = new HighSchoolDiplomaService();
var relocationService = new RelocationService();
var certificationsService = new CertificationsService();
var nameService = new NameService();
var emailService = new EmailService();
var roleService = new RoleService();
var subscriptionService = new SubscriptionService();
var jobLevelService = new JobLevelService();
var jobTypeService = new JobTypeService();
var degreeService = new DegreeService();
var passwordService = new PasswordService();
var jobHistoryService = new JobHistoryService();
var salaryRequirementsService = new SalaryRequirementsService();
var allowedViewersService = new AllowedViewersService();
var employerSearchResultsService = new EmployerSearchResultsService();

var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

router.get('/accounts/:accountId/accountDto', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  accountDtoService(userId).then( (accountDto) =>{
    res.status(200).json({
            success: true,
            message: accountDto
          });
  }).catch((message) => {
    res.status(404).json({
            success: true,
            message: message
          });
  });;
});

router.get('/accounts/:accountId/skills/:skillId/yearsExperience', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  //Example of locking down the get
  var userId = req.params.accountId;
  var skillId = req.params.skillId;

  Account.findOne({
        _id: userId
    }, function (err, account) {
        if (err) {

            res.status(404).json({
              success: false,
              message: 'Unable to find user.'
            });
        }
        else {
          var yearsExp = calculateYearsExperience(account, skillId);

          res.status(200).json({
            success: true,
            message: yearsExp
          });
        }
    });
});

router.get('/accounts/:accountId/previousSearches', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  //Example of locking down the get
  var userId = req.params.accountId;
  Account.findOne({
        _id: userId
    }, function (err, account) {
        if (err) {

            res.status(404).json({
              success: false,
              message: 'Unable to find user.'
            });
        }
        else {
          res.status(200).json({
            success: true,
            message: account.previousSearches
          });
        }
    });
});

router.get('/accounts/:accountId/profilePic', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  //Example of locking down the get
  var userId = req.params.accountId;
  profilePicService.getProfilePic(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.put('/accounts/:accountId/profilePic', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  //Leaving this as an object in the body because it is just a string.
  var profilePic = req.body.profilePicUrl;

  if (!userId || !profilePic) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    profilePicService.editProfilePic(userId, profilePic, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }

});

router.get('/accounts/:accountId/savedSearches', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  //Example of locking down the get
  var userId = req.params.accountId;
  saveSearchService.getSavedSearches(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.put('/accounts/:accountId/saveSearch', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;

  //Leaving this as an object in the body because it is just a string.
  var query = req.body.query;
  var name = req.body.name;

  if (!userId || !query || !name) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    saveSearchService.addSaveSearch(userId, query, name, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }

});

router.get('/accounts/:accountId/openForJobs/', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  openForJobsService.getOpenForJobs(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.put('/accounts/:accountId/openForJobs', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  //Leaving this as an object in the body because it is just a boolean.
  var openForJobs = req.body.openForJobs;

  if (!userId || !firstName || !lastName) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    openForJobsService.editOpenForJobs(userId, openForJobs, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/highSchoolDiploma', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  highSchoolDiplomaService.getHighSchoolDiploma(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.put('/accounts/:accountId/highSchoolDiploma', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  //Leaving this as an object in the body because it is just a boolean.
  var highSchoolDiploma = req.body.highSchoolDiploma;

  if (!userId || !firstName || !lastName) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    highSchoolDiplomaService.editHighSchoolDiploma(userId, highSchoolDiploma, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });

  }
});

router.get('/accounts/:accountId/interestedInRelocation', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  relocationService.getRelocation(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.put('/accounts/:accountId/interestedInRelocation', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  //Leaving this as an object in the body because it is just a boolean.
  var relocation = req.body.interestedInRelocation;

  if (!userId || !firstName || !lastName) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    relocationService.editRelocation(userId, relocation, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });

  }
});

router.get('/accounts/:accountId/employerResults/:queryStr/:skip/:limit', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  var simpleStr = req.params.queryStr;
  var skip = parseInt(req.params.skip);
  var limit = parseInt(req.params.limit);
  var searchItems = employerSearchResultsService.getSimpleResults(userId, simpleStr)
  
  employerSearchResultsService.getResults(userId, "", searchItems, skip, limit, true, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.post('/accounts/:accountId/employerResults', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  var simpleStr = req.body.queryStr;
  var skip = parseInt(req.body.start);
  var limit = parseInt(req.body.length);
  var searchItems = req.body.keyValues;
  if((simpleStr && simpleStr.length > 0) || (searchItems && searchItems.length > 0)) {
    employerSearchResultsService.getResults(userId, simpleStr, searchItems, skip, limit, false, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
  else {
    res.status(200).json({
        success: false,
        message: "invalid usage"
      });
  }
});

function customFilter(object, result, propStr){
    if(!propStr) {
      propStr = [];
    }
    if(object.hasOwnProperty('$in')) {
        propStr.push("$in");
        result.push(propStr.slice());
        propStr.splice(1);
    }

    for(var i=0;i<Object.keys(object).length;i++){
        if(typeof object[Object.keys(object)[i]]=="object"){
            if(i === 0 || propStr.length === 1) {
              propStr.push(Object.keys(object)[i]);
            }
            customFilter(object[Object.keys(object)[i]],result, propStr);
        }
    }
    propStr.splice(1);
}

router.post('/accounts/:accountId/employerResultsFromQuery', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {

  RegExp.prototype.toJSON = RegExp.prototype.toString;

  var userId = req.params.accountId;
  var skip = parseInt(req.body.start);
  var limit = parseInt(req.body.length);
  var queryStr = req.body.query;
  if(queryStr && queryStr.length > 0) {
    var query = JSON.parse(queryStr);

    //Convert regex strings
    var inQueries = [];
    customFilter(query, inQueries);


    var iter = query;
    
    inQueries.forEach((inQuery) =>{
      inQuery.forEach((str) => {
        iter = iter[str];
      });
    });

    for(var i = 0; i < iter.length; i++) {
      if(typeof iter[i] === 'string' ) {
        var parts = /\/(.*)\/(.*)/.exec(iter[i]);
        iter[i] = new RegExp(parts[1], parts[2]);
      }
    }

    employerSearchResultsService.getResultsFromQuery(userId, query, skip, limit, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
  else {
    res.status(200).json({
        success: false,
        message: "invalid usage"
      });
  }
});

router.get('/accounts/:accountId/name', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  nameService.getName(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.put('/accounts/:accountId/name', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  var firstName = req.body.firstName;
  var lastName = req.body.lastName;

  if (!userId || !firstName || !lastName) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    nameService.editName(userId, firstName, lastName, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});


router.put('/accounts/:accountId/jobLevels', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  var jobLevels = req.body;
  var validJobLevel = arrayContainsAll(possibleJobLevels, jobLevels);

  if (!userId || !jobLevels || !validJobLevel) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    jobLevelService.editLevels(userId, jobLevels, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/jobLevels', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;

  jobLevelService.getLevels(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.put('/accounts/:accountId/currentLivingLocations', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  //Leaving this as an object in the body because it is just a string array.
  var currentLivingLocation = req.body;

  if (!userId || !currentLivingLocation) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    currentLivingLocationService.editCurrentLivingLocation(userId, currentLivingLocation, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/currentLivingLocations', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  currentLivingLocationService.getCurrentLivingLocation(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});


router.put('/accounts/:accountId/countriesEligibility', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  //Leaving this as an object in the body because it is just a string array.
  var countriesEligibility = req.body.countriesEligibility;

  if (!userId || !countriesEligibility) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    countriesEligibilityService.editCountriesEligibility(userId, countriesEligibility, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/countriesEligibility', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  countriesEligibilityService.getCountriesEligibility(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.put('/accounts/:accountId/portfolio', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  //Leaving this as an object in the body because it is just a string array.
  var portfolio = req.body.portfolio;

  if (!userId || !portfolio) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    portfolioService.editPortfolio(userId, portfolio, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }

});

router.get('/accounts/:accountId/portfolio', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  portfolioService.getPortfolio(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.get('/accounts/:accountId/skills', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  skillsService.getSkills(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.delete('/accounts/:accountId/cleanSkills', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  skillsService.cleanSkills(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.post('/accounts/:accountId/certifications', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var certification = req.body;

  if (!userId || !certification) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    certificationsService.addCertification(userId, certification, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/accounts/:accountId/certifications', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var certification = req.body;

  if (!userId || !certification) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    certificationsService.editCertification(userId, certification, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }

});

router.delete('/accounts/:accountId/certifications/:certificationId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var certificationId = req.params.certificationId;

  if (!userId || !certificationId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    certificationsService.removeCertification(userId, certificationId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/certifications/', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  certificationsService.getCertifications(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});


router.put('/accounts/:accountId/jobTypes', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var jobTypes = req.body;
  var validJobType = arrayContainsAll(possibleJobTypes, jobTypes);

  if (!userId || !jobTypes || !validJobType) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    jobTypeService.editJobType(userId, jobTypes, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/jobTypes/', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  jobTypeService.getTypes(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

/**
 * Using Authentication, change the user's password.
 */
router.put('/accounts/:accountId/changePassword', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var currentPw = req.body.currentPassword;
  var newPw = req.body.newPassword;
  var repeatPw = req.body.repeatPassword;

  if (userId && currentPw && newPw && repeatPw) {

    passwordService.changePassword(userId, currentPw, newPw, repeatPw, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });

  } else {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
});

router.post('/accounts/:accountId/degrees', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var degree = req.body;

  if (!userId || !degree) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    degreeService.addDegree(userId, degree, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/accounts/:accountId/degrees', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var degree = req.body;

  if (!userId || !degree) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    degreeService.editDegree(userId, degree, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.delete('/accounts/:accountId/degrees/:degreeId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var degreeId = req.params.degreeId;

  if (!userId || !degreeId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    degreeService.removeDegree(userId, degreeId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/degrees', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  degreeService.getDegrees(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.post('/accounts/:accountId/jobHistory', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var job = req.body;

  if (!userId || !job) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    var skillNames = job.skills;
    jobHistoryService.addSkills(userId, skillNames, () =>{
      jobHistoryService.addJobHistory(userId, job, skillNames, (message) => {
        res.status(message.statusCode).json({
          success: message.success,
          message: message.message
        });
      });
    });
  }
});

router.put('/accounts/:accountId/jobHistory', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var job = req.body;

  if (!userId || !job) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    var skillNames = job.skills;
    jobHistoryService.addSkills(userId, skillNames, () =>{
      jobHistoryService.editJobHistory(userId, job, skillNames, (message) => {
        res.status(message.statusCode).json({
          success: message.success,
          message: message.message
        });
      });
    });
  }
});

router.delete('/accounts/:accountId/jobHistory/:jobHistoryId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var jobHistoryId = req.params.jobHistoryId;

  if (!userId || !jobHistoryId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    jobHistoryService.removeJobHistory(userId, jobHistoryId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }

});

router.get('/accounts/:accountId/jobHistory/', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  jobHistoryService.getJobHistory(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.post('/accounts/:accountId/email', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  //Leaving this as an object in the body because it is just a string.
  var email = req.body.email;

  emailService.editEmail(userId, email, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

router.get('/accounts/:accountId/email', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  emailService.getEmail(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.get('/accounts/:accountId/role', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  roleService.getRole(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.post('/accounts/:accountId/subscription', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var subscription = req.body;

  var validIndex = possibleSubscriptionTypes.indexOf(subscription) >= 0;

  if (!validIndex) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
  else {
    subscriptionService.editSubscription(userId, subscription, (message) => {
      //TODO:  Need to catch any errors from stripe here if applicable.
      if (!success) {
        //TODO: Handle errors here.
      }
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });

  }
});

router.get('/accounts/:accountId/subscription', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;

  subscriptionService.getSubscription(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.post('/accounts/:accountId/salaryRequirements', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var salaryRequirement = req.body;

  if (!userId || !salaryRequirement) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    salaryRequirementsService.addSalaryRequirement(userId, salaryRequirement, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/accounts/:accountId/salaryRequirements', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var salaryRequirement = req.body;

  if (!userId || !salaryRequirement) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    salaryRequirementsService.editSalaryRequirement(userId, salaryRequirement, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.delete('/accounts/:accountId/salaryRequirements/:salaryRequirementId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var salaryRequirementId = req.params.salaryRequirementId;

  if (!userId || !salaryRequirementId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    salaryRequirementsService.removeSalaryRequirement(userId, salaryRequirementId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/salaryRequirements', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Default), function (req, res) {
  var userId = req.params.accountId;
  salaryRequirementsService.getSalaryRequirements(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });

});

router.put('/accounts/:accountId/allowedViewers/:allowedViewerId/', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var allowedAccountId = req.params.allowedViewerId;
  var allowView = req.body.allowView || false;

  if (!userId || !allowedAccountId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    allowedViewersService.editAllowedViewer(userId, allowedAccountId, allowView, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.post('/accounts/:accountId/allowedViewers', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Recruiter), function (req, res) {
  var userId = req.params.accountId;
  var allowedAccountId = req.body.accountId;

  if (!userId || !allowedAccountId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    allowedViewersService.addAllowedViewer(userId, allowedAccountId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.delete('/accounts/:accountId/allowedViewers/:allowedAccountId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  var allowedAccountId = req.params.allowedAccountId;

  if (!userId || !allowedAccountId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    allowedViewersService.removeAllowedViewer(userId, allowedAccountId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/accounts/:accountId/allowedViewers', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var userId = req.params.accountId;
  allowedViewersService.getAllowedViewers(userId, (message) => {
    res.status(message.statusCode).json({
      success: message.success,
      message: message.message
    });
  });
});

module.exports = router;