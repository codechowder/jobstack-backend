var express = require('express');
var passport = require("passport");

var router = express.Router();

router.post('/', function (req, res, next) {
    // parse the event
    var event_json = JSON.parse(req.body);
    // Verify the event by fetching it from Stripe
    stripe.events.retrieve(event_json.id, function (err, event) {
        // Do something with event
        switch (event.type) {
            case 'customer.subscription.created':
                break;
            case 'customer.subscription.deleted':
                break;
            case 'customer.subscription.updated':
                break;
            case 'invoice.created':
                break;
            case 'charge.succeeded':
                break;
            case 'charge.failed':
                break;
            case 'invoice.payment_succeeded':
                break;
            case 'invoice.payment_failed':
                break;
            case 'ping':
                break;
        }
        res.send(200);
    });
});

module.exports = router;