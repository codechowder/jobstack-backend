var express = require('express');
var passport = require("passport");
var router = express.Router();

var message = require("../models/message");
var allowedAccessEnum = require("../models/allowAccessEnum");
var Account = require("../models/accounts").model;

var allowAccess = require("../serviceLayer/companyServices/allowAccess");
var AssociatedRecruitersService = require("../serviceLayer/companyServices/associatedRecruitersService");
var DescriptionService = require("../serviceLayer/companyServices/descriptionService");
var LocationService = require("../serviceLayer/companyServices/locationService");
var LogoService = require("../serviceLayer/companyServices/logoService");
var NameService = require("../serviceLayer/companyServices/nameService");
var SubscriptionUserLimitService = require("../serviceLayer/companyServices/subscriptionUserLimitService");
var WebsiteUrlService = require("../serviceLayer/companyServices/websiteUrlService");
var CompanyService = require("../serviceLayer/companyServices/companyService");

var associatedRecruitersService = new AssociatedRecruitersService();
var descriptionService = new DescriptionService();
var locationService = new LocationService();
var logoService = new LogoService();
var nameService = new NameService();
var subscriptionUserLimitService = new SubscriptionUserLimitService();
var websiteUrlService = new WebsiteUrlService();
var companyService = new CompanyService();

/* company Service Calls Start */
router.get('/companies/', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Recruiter), function (req, res) {
  //Example of locking down the get
  var currentUserId = req.user.id;
  if (!currentUserId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
  else {
    companyService.getCompanies(currentUserId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/companies/all', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.All), function (req, res) {
  //Example of locking down the get
  var currentUserId = req.user.id;
  if (!currentUserId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
  else {
    companyService.getAllCompanies((message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/companies/:companyId/userRequests', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Recruiter), function (req, res) {
  var companyId = req.params.companyId;
  var currentUserId = req.user.id;
  if (!companyId || !currentUserId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
  else {
    companyService.getUserRequests(companyId, currentUserId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.get('/companies/:companyId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.Recruiter), function (req, res) {
  //Example of locking down the get
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
  else {
    companyService.getCompany(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.post('/companies', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.UserOnly), function (req, res) {
  var company = req.body;
  var currentUserId = req.user.id;
  if (!company) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    companyService.addCompany(company, currentUserId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.delete('/companies/:companyId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    companyService.removeCompany(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* company Service Calls End */

/* associatedRecruiters Service Calls Start */
router.get('/companies/:companyId/recruiters', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  //Example of locking down the get
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  }
  else {
    associatedRecruitersService.getAssociatedRecruiters(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.post('/companies/:companyId/recruiters/:accountId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var userId = req.params.accountId;
  var companyId = req.params.companyId;
  var allowed = req.body.allowed;

  if (!companyId || !userId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    associatedRecruitersService.addAssociatedRecruiter(companyId, userId, allowed, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/recruiters/:accountId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var userId = req.params.accountId;
  var companyId = req.params.companyId;
  var allowed = req.body.allowed;

  if (!companyId || !userId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    associatedRecruitersService.editAssociatedRecruiter(companyId, userId, allowed, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.delete('/companies/:companyId/recruiters/:accountId', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var userId = req.params.accountId;
  var companyId = req.params.companyId;

  if (!companyId || !userId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    associatedRecruitersService.removeAssociatedRecruiter(companyId, userId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* associatedRecruiters Service Calls End */

/* Name Service Calls Start */
router.get('/companies/:companyId/name', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    nameService.getName(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/name', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  var name = req.body.name;

  if (!companyId || !name) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    nameService.editName(companyId, name, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* Name Service Calls End */

/* Description Service Calls Start */
router.get('/companies/:companyId/description', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    descriptionService.getDescription(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/description', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  var description = req.body.description;

  if (!companyId || !description) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    descriptionService.editDescription(companyId, description, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* Description Service Calls End */

/* websiteUrl Service Calls Start */
router.get('/companies/:companyId/websiteUrl', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    websiteUrlService.getWebsiteUrl(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/websiteUrl', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  var websiteUrl = req.body.websiteUrl;

  if (!companyId || !websiteUrl) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    websiteUrlService.editWebsiteUrl(companyId, websiteUrl, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* websiteUrl Service Calls End */

/* location Service Calls Start */
router.get('/companies/:companyId/location', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    locationService.getLocation(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/location', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  var location = req.body.location;

  if (!companyId || !location) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    locationService.editLocation(companyId, location, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* location Service Calls End */

/* logo Service Calls Start */
router.get('/companies/:companyId/logo', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    logoService.getLogo(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/logo', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  var logo = req.body.logo;

  if (!companyId || !logo) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    logoService.editLogo(companyId, logo, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* logo Service Calls End */

/* subscriptionUserLimit Service Calls Start */
router.get('/companies/:companyId/subscriptionUserLimit', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.AdminOnly), function (req, res) {
  var companyId = req.params.companyId;
  if (!companyId) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    subscriptionUserLimitService.getSubscriptionUserLimit(companyId, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});

router.put('/companies/:companyId/subscriptionUserLimit', passport.authenticate('jwt', {
  session: false
}), allowAccess(allowedAccessEnum.CompanyOnly), function (req, res) {
  var companyId = req.params.companyId;

  var subscriptionUserLimit = req.params.subscriptionUserLimit;

  if (!companyId || !subscriptionUserLimit) {
    res.status(500).json({
      success: false,
      message: "Invalid Usage."
    });
  } else {
    subscriptionUserLimitService.editSubscriptionUserLimit(companyId, subscriptionUserLimit, (message) => {
      res.status(message.statusCode).json({
        success: message.success,
        message: message.message
      });
    });
  }
});
/* subscriptionUserLimit Service Calls End */

module.exports = router;