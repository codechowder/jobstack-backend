var express = require('express');
var _ = require("lodash");
var bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
var Account = require("../models/accounts").model;
let JobHistory = require("../models/jobhistory").model;
var calculateYearsExperience = require("../businessLogic/calculateYearsExperience");
var jwtTokenGeneration = require("../businessLogic/jwtTokenGeneration");
var passport = require("passport");
var passportJWT = require("passport-jwt");
var request = require('request');
var accountDto = require("../models/frontendModels/accountDto");
let FB = require('fb');
let config = require('../config/');

var router = express.Router();
var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var _requestSecret;

function loginOrCreateUser(user, cb) {
    if (user.isNew) {
        user.save((err) => {
            //TODO:  The backend should not throw the error when we deploy.
            if (err) {
                cb(err);
            }
            else {
                user.totalYearsExperience = calculateYearsExperience(user, -1);
                user.skills.forEach((s) => {
                    s.totalYearsExperience = calculateYearsExperience(user, s._id);
                });
                user.save((err) => {
                    if (err) {
                        cb(err);
                    }
                    else {
                        cb(null);
                    }
                });

            }
        });

    }
    else {
        cb(null);
    }
}

function LoginSocialMedia(req, res) {
    if (req.user) {

        loginOrCreateUser(req.User, function (err) {
            if (err) throw err;

            jwtTokenGeneration(req.user).then((obj) => {

                res.status(200).json({
                    success: true,
                    message: 'Authentication successfull',
                    token: obj.token,
                    user: obj.accountDto
                });
            });
        });
    } else {
        res.status(400).send({
            success: false,
            message: 'Authentication failed. Passwords did not match.'
        });
    }
}

/**
 * Registers an account.
 */
router.post('/register', function (req, res) {
    if (!req.body.email || !req.body.password) {
        res.status(200).json({
            success: false,
            message: 'Please enter email and password.'
        });
    } else {
        var isRecruiter = req.body.isRecruiter;
        var roles = [];
        if (isRecruiter) {
            roles.push("recruiter");
        }
        else {
            roles.push("user");
        }
        var newAccount = new Account({
            email: req.body.email,
            password: req.body.password,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            roles: roles
        });

        // Attempt to save the user
        newAccount.save(function (err, account) {
            if (err) {
                res.status(200).json({
                    success: false,
                    message: 'That email already exists.'
                });
            } else {
                // Do not send password with account info.
                account.password = null;
                jwtTokenGeneration(account).then((obj) => {
                    res.status(200).json({
                        success: true,
                        token: obj.token,
                        user: obj.accountDto,
                        message: 'Successfully created new user.'
                    });
                });
            }
        });
    }
});

router.post('/linkedin', function (req, res) {
    var profieId = req && req.body && req.body.id || null;

    var isRecruiter = req.body.isRecruiter;
    var roles = [];
    if (isRecruiter) {
        roles.push("recruiter");
    }
    else {
        roles.push("user");
    }

    if (profileId) {
        loginSocialMediaAndRespond({ linkedInId: profileId }, function () {
            var newAccount = new Account({
                linkedInId: profileId,
                profilePic: req.body.pictureUrl || "",
                firstName: req.body.firstName || "",
                lastName: req.body.lastName || "",
                // TODO: Do we need to save this to S3 or something?
                // TODO: This needs to be fixed before we deploy.
                email: req.body.emailAddress || "",
                roles: roles
            });

            if (req.body.positions && !isRecruiter) {
                req.body.positions.values.forEach((work) => {
                    var jobHistory = new JobHistory({
                        companyName: work.company && work.company.name || "",
                        fromDate: work.startDate && work.startDate.year && work.startDate.month && new Date(work.startDate.year, work.startDate.month) || new Date(Date.now()),
                        toDate: work.endDate && work.endDate.year && work.endDate.month && new Date(work.endDate.year, work.endDate.month) || new Date(Date.now()),
                        isCurrent: work.isCurrent,
                        description: work.summary,
                        position: work.title,
                        skills: []
                    });

                    newAccount.jobHistory.push(jobHistory);
                });

                loginOrCreateUserAndRespond(newAccount, res);
            }
        }, res);
    }
    else {
        res.status(400).send({
            success: false,
            message: "Credentials not found."
        });
    }
});

router.post('/google', function (req, res) {

    var profileId = req && req.body && req.body.metadata && req.body.metadata.sources && req.body.metadata.sources[0] || null;
    
    var isRecruiter = req.body.isRecruiter;
    var roles = [];
    if (isRecruiter) {
        roles.push("recruiter");
    }
    else {
        roles.push("user");
    }

    if (profileId) {
        loginSocialMediaAndRespond({ googleId: profileId }, function () {
            var newAccount = new Account({
                googleId: profileId,
                profilePic: req.body.photos && req.body.photos[0] && req.body.photos[0].url || "",
                firstName: req.body.names && req.body.names[0] && req.body.names[0].givenName || "",
                lastName: req.body.names && req.body.names[0] && req.body.names[0].familyName || "",
                // TODO: Do we need to save this to S3 or something?
                // TODO: This needs to be fixed before we deploy.
                emailAddress: req.body.emailAddresses && req.body.emailAddresses[0] && req.body.emailAddresses[0].value,
                roles: roles
            });

            loginOrCreateUserAndRespond(newAccount, res);
        }, res);
    }
    else {
        res.status(400).send({
            success: false,
            message: "Credentials not found."
        });
    }
});

router.post('/facebook', function (req, res) {
    var profileId = req && req.body && req.body.userID || null;
    var accessToken = req && req.body && req.body.accessToken || null;

    var isRecruiter = req.body.isRecruiter;
    var roles = [];
    var fields = [];
    if (isRecruiter) {
        roles.push("recruiter");
        fields = ["id", "name", "first_name", "last_name", "email", "picture"];
    }
    else {
        roles.push("user");
        fields = ["id", "name", "first_name", "last_name", "email", "picture", "work"];
    }

    if (profileId && accessToken) {
        loginSocialMediaAndRespond({ facebookId: profileId }, function () {
            FB.setAccessToken(accessToken);
            FB.api("/me", {
                fields: fields
            }, function (response) {
                if (response && !response.error) {
                    var newAccount = new Account({
                        facebookId: response.id,
                        firstName: response.first_name,
                        lastName: response.last_name,
                        // TODO: Do we need to save this so SF3 or something?
                        profilePic: response.picture.data.url,
                        email: response.email,
                        roles: roles
                    });

                    if(!isRecruiter) {
                        response.work.forEach(function (work) {
                            if (work) {
                                var jobHistory = new JobHistory({
                                    companyName: work.employer && work.employer.name || "",
                                    fromDate: work.start_date,
                                    toDate: work.end_date,
                                    isCurrent: !work.end_date,
                                    position: work.position && work.position.name || "",
                                    skills: []
                                });
                                newAccount.jobHistory.push(jobHistory);
                            }
                        });
                    }

                    loginOrCreateUserAndRespond(newAccount, res);
                }
            });
        }, res);
    }
    else {
        res.status(400).send({
            success: false,
            message: "Credentials not found."
        });
    }
});

/**
 * Authenticate to the facebook api.
 */
router.get('/facebook', passport.authenticate('facebook', {
    scope: ['email', 'user_work_history']
}));

/**
 * The facebook callback endpoint which facebook will call back after success.
 */
router.get('/facebook/callback', passport.authenticate('facebook', {
    session: false,
    failureRedirect: '/',
    scope: 'email'
}), function (req, res) {
    LoginSocialMedia(req, res);
});

/** 
 * Authenticate the user and get a JSON Web Token to include in the header of future requests.
 */
router.post('/auth', (req, res) => {
    var email = req.body.email;
    var password = req.body.password;

    if ((!email || !password) && password.length > 0) {
        res.status(500).json({
            success: false,
            message: 'Authentication failed. Unknown Error.'
        });
    }

    Account.findOne({
        email: email
    }, function (err, account) {
        //TODO:  The backend should not throw the error when we deploy.
        if (err) {
            res.status(500).json({
                success: false,
                message: 'Authentication failed. Unknown Error.'
            });
        }
        if (!account) {
            res.status(200).json({
                success: false,
                message: 'Authentication failed. User not found.'
            });
        } else {
            // Check if password matches
            account.comparePassword(password, function (err, isMatch) {
                if (isMatch && !err) {

                    // Create token if the password matched and no error was thrown
                    jwtTokenGeneration(account).then((obj) => {
                        res.status(200).json({
                            success: true,
                            message: 'Authentication successfull',
                            token: obj.token,
                            user: obj.accountDto
                        });
                    });
                } else {
                    res.status(200).json({
                        success: false,
                        message: 'Authentication failed. Passwords did not match.'
                    });
                }
            });
        }
    });
});

/**
 * Logs a user out.
 */
router.get('/logout', function (req, res) {
    req.logout();
    res.status(200).json({
        success: true
    });
});

/**
 * Ping endpoint for testin purposes.
 */
router.get('/ping', passport.authenticate('jwt', {
    session: false
}), function (req, res) {
    res.status(200).send("pong!");
});

/**
 * REGION: Private Methods
 */

function loginOrCreateUserAndRespond(user, res) {
    if (user) {
        loginOrCreateUser(user, function (error) {
            if (error) {
                res.status(401).json({
                    success: false,
                    message: "Authentication failed."
                });
            }
            else {
                // Do not send password with account info.
                user.password = null;
                jwtTokenGeneration(user).then((obj) => {
                    if (obj.token) {
                        res.status(200).json({
                            success: true,
                            message: "Authentication successful.",
                            token: obj.token,
                            user: obj.accountDto
                        });

                    }
                    else {
                        res.status(500).json({
                            success: false,
                            message: "Token generation failed."
                        });
                    }
                });
            }
        });
    }
    else {
        res.status(500).json({
            success: false,
            message: "User not found."
        });
    }
}

function loginSocialMediaAndRespond(accountKeyObj, socialMediaAccountCreateFunc, res) {
    Account.findOne(accountKeyObj, function (error, user) {
        if (error) {
            res.status(401).json({
                success: false,
                message: "Authentication failed."
            });
        }
        else if (user) {
            loginOrCreateUserAndRespond(user, res);
        }
        else {
            try {
                socialMediaAccountCreateFunc();
            }
            catch (e) {
                res.status(500).json({
                    success: false,
                    message: "Account creation failed."
                });
            }
        }
    });
}

/**
 * ENDREGION: Private Methods
 */

module.exports = router;