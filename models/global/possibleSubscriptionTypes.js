/**
 * Enumeration for possible Subscription Types
 */
var possibleSubscriptionTypes = ['basic', 'premium'];

module.exports = possibleSubscriptionTypes;