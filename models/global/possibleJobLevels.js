/**
 * Enumeration for Possible Job Levels
 */
var possibleJobLevels = ['intern', 'junior', 'midlevel', 'senior', 'management', 'director', 'cxo'];

module.exports = possibleJobLevels;