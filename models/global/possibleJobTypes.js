/**
 * Enumeration for possible Job Types
 */
var possibleJobTypes = ['fulltime', 'parttime', 'contract', 'internship', 'remote', 'freelance', 'profitbased'];

module.exports = possibleJobTypes;