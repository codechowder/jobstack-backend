/**
 * Enumerations for Possible Roles
 */
var possibleRoles = ['default', 'user', 'recruiter', 'company', 'admin'];

module.exports = possibleRoles;