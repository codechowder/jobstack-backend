var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Schema for degrees.
 */
var CertificationSchema = new Schema({

    acquired: Date,
    name: String,
    expiration: Date,
    code: String
});

var Certification = mongoose.model('Certification', CertificationSchema);

// make this available to our users in our Node applications
module.exports = {
    model: Certification,
    schema: CertificationSchema
};