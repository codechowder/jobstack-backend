var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');
var degree = require('../models/degrees');
var salaryreqs = require('../models/salaryrequirements');
var jobhistory = require('../models/jobhistory');
var skills = require("../models/skill");
var certifications = require("../models/certification");

var possibleRoles = require('../models/global/possibleRoles');
var possibleJobLevels = require('../models/global/possibleJobLevels');
var possibleJobTypes = require('../models/global/possibleJobTypes');
var possibleSubscriptionTypes = require('../models/global/possibleSubscriptionTypes');

/**
 * Schema for User accounts.
 */
var AccountSchema = new Schema({
  nonce: Schema.Types.ObjectId, //this is used for protecting against concurrent edits: http://docs.mongodb.org/ecosystem/use-cases/metadata-and-asset-management/
  password: {
    type: String,
    //required: true
  },
  roles: {
    type: [{
      type: String,
      enum: possibleRoles
    }],
    default: 'default'
  },
  interestedInRelocation: Boolean,
  countriesEligibility: [String],
  profilePic: String,
  currentLivingLocation: [String],
  skills: [skills.schema],
  portfolio: [String],
  // maybe need to put this into a social object.
  facebookId: String,
  twitterId: String,
  googleId: String,
  linkedInId: String,
  stripeCustomerId: String,
  email: {
    type: String,
    lowercase: true,
    required: true,
    unique: true,
    trim: true
  },
  firstName: String,
  lastName: String,
  openForJobs: {
    type: Boolean,
    default: true
  },
  highSchoolDiploma: Boolean,
  certifications: [certifications.schema],
  updated: {
    type: Date,
    default: Date.now
  },
  jobLevels: {
    type: [{
      type: String,
      enum: possibleJobLevels
    }]
  },
  jobTypes: {
    type: [{
      type: String,
      enum: possibleJobTypes
    }]
  },
  salaryRequirements: [salaryreqs.schema],
  degrees: [degree.schema],
  jobHistory: [jobhistory.schema],
  totalYearsExperience: Number,
  //TODO:  Should this be a seperate object?
  allowedViewers: [{
    expires: Date,
    allowView: Boolean,
    allowedViewer: {
      type: Schema.Types.ObjectId,
      ref: 'Account'
    }
  }],
  subscription: {
    type: [{
      type: String,
      enum: possibleSubscriptionTypes
    }],
    default: 'basic'
  },
  savedSearches: [{
    query: String,
    name: String
  }],
  previousSearches: [{
    query: String,
    date: Date
  }]
});

// Hash the user's password before inserting a new user
AccountSchema.pre('save', function (next) {
  var user = this;
  if ((this.isModified('password') || this.isNew) && this.password && this.password.length > 0) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

// Compare password input to password saved in database
AccountSchema.methods.comparePassword = function (pw, cb) {
  bcrypt.compare(pw, this.password, function (err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

AccountSchema.plugin(passportLocalMongoose);

var Account = mongoose.model('Account', AccountSchema);

module.exports = {
  model: Account,
  schema: AccountSchema
}