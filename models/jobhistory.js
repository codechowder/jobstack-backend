var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JobHistorySchema = new Schema({
    companyName: String,
    fromDate: Date,
    toDate: Date,
    description: String,
    position: String,
    isCurrent: Boolean,
    skills: [{
        type: Schema.Types.ObjectId,
        ref: 'Skill'
    }]
});

var JobHistory = mongoose.model('JobHistory', JobHistorySchema);

// make this available to our users in our Node applications
module.exports = {
    model: JobHistory,
    schema: JobHistorySchema
};