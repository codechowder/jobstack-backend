var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var possibleDiplomaTypes = ['associate', 'bachelor', 'graduate', 'postgraduate'];

/**
 * Schema for degrees.
 */
var DegreeSchema = new Schema({
    degreeType: {
        type: [{
            type: String,
            enum: possibleDiplomaTypes
        }]
    },
    school: String,
    degree: String,
    fieldOfStudy: String,
    fromDate: Date,
    toDate: Date
});

var Degree = mongoose.model('Degree', DegreeSchema);

// make this available to our users in our Node applications
module.exports = {
    model: Degree,
    schema: DegreeSchema
};