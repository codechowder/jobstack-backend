var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Schema for company.
 */
var CompanySchema = new Schema({
    name: String,
    description: String,
    websiteUrl: String,
    location: String,
    logo: String,
    updated: {
        type: Date,
        default: Date.now
    },
    recruiters: [{
        allow: Boolean,
        recruiter: {
            type: Schema.Types.ObjectId,
            ref: 'Account'
        }
    }],
    subscriptionUserLimit: {
        type: Number,
        min: 0,
        default: 0
    }
});

var Company = mongoose.model('Company', CompanySchema);

// make this available to our users in our Node applications
module.exports = {
    model: Company,
    schema: CompanySchema
};