var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Schema for degrees.
 */
var SkillSchema = new Schema({

    skill: {
        type: String,
        lowercase: true, 
        trim: true,
        unique: true
    },
    totalYearsExperience: Number
});

var Skill = mongoose.model('Skill', SkillSchema);

// make this available to our users in our Node applications
module.exports = {
    model: Skill,
    schema: SkillSchema
};