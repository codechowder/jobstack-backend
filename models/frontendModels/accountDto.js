var Account = require("../../models/accounts").model;

var accountDto = function(account, fullLoad = false){
    this.id = account.id;
    this.email = account.email;
    this.roles = account.roles;
    this.profilePic = account.profilePic;
    this.subscription = account.subscription;

    this.jobLevels = account.jobLevels;
    this.jobTypes = account.jobTypes;
    this.countriesEligibility = account.countriesEligibility;
    this.currentLivingLocation = account.currentLivingLocation;
    this.skills = account.skills;
    
    this.firstName = account.firstName;
    this.lastName = account.lastName;
    this.openForJobs = account.openForJobs;
    this.highSchoolDiploma = account.highSchoolDiploma;
    this.salaryRequirements = account.salaryRequirements;

    if(fullLoad) {
        this.portfolio = account.portfolio;
        this.certifications = account.certifications;
        this.degrees = account.degrees;
        this.jobHistory = account.jobHistory;
    }
    // this.savedSearches = account.savedSearches;
    // this.previousSearches = account.previousSearches;
}

accountDto.prototype.id = function(){
    return this.id;
};

accountDto.prototype.email = function(){
    return this.email;
};

accountDto.prototype.roles = function(){
    return this.roles;
};

accountDto.prototype.profilePic = function(){
    return this.profilePic;
};

accountDto.prototype.subscription = function(){
    return this.subscription;
};

accountDto.prototype.jobTypes = function(){
    return this.jobTypes;
}

accountDto.prototype.jobLevels = function(){
    return this.jobLevels;
}

accountDto.prototype.jobTypes = function(){
    return this.jobTypes;
}

accountDto.prototype.countriesEligibility = function(){
    return this.countriesEligibility;
}

accountDto.prototype.currentLivingLocation = function(){
    return this.currentLivingLocation;
}

accountDto.prototype.skills = function(){
    return this.skills;
}

accountDto.prototype.portfolio = function(){
    return this.portfolio;
}

accountDto.prototype.firstName = function(){
    return this.firstName;
}

accountDto.prototype.lastName = function(){
    return this.lastName;
}

accountDto.prototype.openForJobs = function(){
    return this.openForJobs;
}

accountDto.prototype.highSchoolDiploma = function(){
    return this.highSchoolDiploma;
}

accountDto.prototype.certifications = function(){
    return this.certifications;
}

accountDto.prototype.salaryRequirements = function(){
    return this.salaryRequirements;
}

accountDto.prototype.degrees = function(){
    return this.degrees;
}

accountDto.prototype.jobHistory = function(){
    return this.jobHistory;
}

// accountDto.prototype.savedSearches = function(){
//     return this.savedSearches;
// }

// accountDto.prototype.previousSearches = function() {
//     return this.previousSearches;
// }

module.exports = accountDto;