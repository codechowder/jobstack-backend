var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SalaryReqsSchema = new Schema({
    location: String,
    salaryRequirement: {
        type: Number,
        min: 0
    }
});

var SalaryReqs = mongoose.model('SalaryRequirements', SalaryReqsSchema);

// make this available to our users in our Node applications
module.exports = {
    model: SalaryReqs,
    schema: SalaryReqsSchema
};