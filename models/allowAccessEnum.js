/**
 * Enumeration for Possible accounts that can get information from RESTful GET calls
 */
var allowedAccessEnum = {
    /**
     * No Restrictions at all for allowed access
     */
    All: 0,
    /**
     * Admins, current users and allowed viewers are allowed access
     */
    Default: 1,
    /**
     * The Current User and Admin are only allowed access
     */
    UserOnly: 2,
    /**
     * Only Admins are only allowed access
     */
    AdminOnly: 3,
    /**
     * Company role are only allowed access
     */
    CompanyOnly: 4,
    /**
     * For Companies - Allow recruiter to have access
     */
    Recruiter: 5
};

module.exports = allowedAccessEnum;