var express = require('express');

module.export = {
    ensureAuthentication: function (req, res, next) {
        if (req.isAuthenticated()) {
            // req.user is available for use here
            return next();
        }
        // denied. redirect to login
        res.status(401);
    }
}