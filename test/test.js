var Account = require("../models/accounts").model;
var arrayContainsAll = require("../businessLogic/arrayContainsAll");

var assert = require('assert');

/**
 * The Business Logic tests for arrayContainsAll
 */
describe('Business Logic Tests', function () {
    describe('arrayContainsAll', function () {
        it('should test for two values in the array and fail.', function () {
            var arrayOfValues = ['cat', 'dog'];
            var arrayToTest = ['steve', 'is', 'a', 'dog'];

            var bPassed = arrayContainsAll(arrayOfValues, arrayToTest);
            assert.equal(false, bPassed);
        });

        it('should test for a one value in the array and pass.', function () {
            var arrayOfValues = ['dog'];
            var arrayToTest = ['steve', 'is', 'a', 'cat', 'dog'];

            var bPassed = arrayContainsAll(arrayOfValues, arrayToTest);
            assert.equal(true, bPassed);
        });

        it('should test for a multiple values in the array and pass.', function () {
            var arrayOfValues = ['dog', 'cat', 'steve'];
            var arrayToTest = ['steve', 'is', 'a', 'cat', 'dog'];

            var bPassed = arrayContainsAll(arrayOfValues, arrayToTest);
            assert.equal(true, bPassed);
        });

        it('should test for no values not in the array and fail.', function () {
            var arrayOfValues = ['food', 'cat', 'dog'];
            var arrayToTest = ['steve', 'is', 'a', 'goober'];

            var bPassed = arrayContainsAll(arrayOfValues, arrayToTest);
            assert.equal(false, bPassed);
        });
    });
});