// dependencies
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var routes = require('./routes/index');
var valueRoutes = require('./routes/values');
var stripeRoutes = require('./routes/stripe');
var auth = require('./routes/authentication');
var company = require('./routes/company');
var config = require('./config');

var app = express();

// CORS
app.use(function(request, response, next) {
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,PATCH,DELETE,OPTIONS");
  var allowedHeaders = [
    "Content-Type",
    "Authorization",
    "Content-Length",
    "X-Requested-By",
    "X-Registered-With"
  ];
  response.setHeader("Access-Control-Allow-Headers", allowedHeaders.join(","));
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(require('express-session')({
  secret: config.auth.sessionSecret,
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// passport config
require('./config/passport')(passport);

// Endpoint routes
app.use('/api', routes);
app.use('/api', company);
app.use('/authentication', auth);
app.use('/values', valueRoutes);
app.use('/stripe', stripeRoutes);


// mongoose
if (app.get('env') === 'development') {
  mongoose.connect(config.database.local);
} else {
  mongoose.connect(config.database.remote);
}
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;