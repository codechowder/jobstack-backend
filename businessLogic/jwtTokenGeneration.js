var jwt = require("jsonwebtoken");
var AccountDTO = require("../models/frontendModels/accountDto");
var config = require("../config");
var skillNamesFromJobHistory = require("../serviceLayer/skillNamesFromJobHistory");

//https://github.com/KevinNTH/express-jwt-mongoose-example
/**
 * Gets the token for auth.
 * @param user 
 */
var jwtTokenGeneration = function (user) {
  var promise = new Promise(
    (resolve, reject) => {
      var accountDto = new AccountDTO(user);

      skillNamesFromJobHistory(accountDto, accountDto.jobHistory);
      var token = jwt.sign(accountDto, config.auth.secret, {
        expiresIn: "2 days"
      });
      resolve({ token: token, accountDto: accountDto });
    });

  return promise;
}

module.exports = jwtTokenGeneration;