var config = require('../config');
var stripe = require('stripe')(config.stripe.testApiKey);

var stripeLogic = function () {}

/**
 * creates a stripe customer and returns the stripe customer
 * @param {String} email customer email address
 * @param {function} cb callback function with err and stripe customer object
 */
stripeLogic.prototype.createCustomer = function (email, cb) {
    // Check for email before proceeding.
    if (!email) {
        return cb('no email provided', null);
    }
    // Create a user on stripe.com
    stripe.customers.create({
        email: email
    }, function (err, customer) {
        return cb(null, customer);
    });
}

/**
 * Edits the stripe customer email on stripe.com
 * @param {String} stripeCustomerId stripe customer id.
 * @param {String} newEmail new email address of the customer.
 * @param {function} cb callback function with err and stripe customer object
 */
stripeLogic.prototype.updateCustomerEmail = function (stripeCustomerId, newEmail, cb) {
    // Check for email before proceeding.
    if (!newEmail) {
        return cb('no email provided', null);
    }

    // Check for user before proceeding.
    if (!stripeCustomerId) {
        return cb('no stripeCustomerId provided to edit.', null);
    }

    // Edit the user on stripe.com
    stripe.customers.update(stripeCustomerId, {
        email: newEmail
    }, function (err, customer) {
        // asynchronously called
    });
}

/**
 * Associates a stripe customer to a plan id.
 * @param {String} stripeCustomerId stripe customer id.
 * @param {String} plandId stripe plan to subscribe the user to.
 * @param {function} cb callback function with err and stripe subscription object
 */
stripeLogic.prototype.associatePlan = function (stripeCustomerId, planId, cb) {
    // Check for user before proceeding.
    if (!stripeCustomerId) {
        return cb('no stripeCustomerId provided to edit.', null);
    }

    if (!planId) {
        return cb('no planId provided to associated', null);
    }

    stripe.subscriptions.create({
        customer: stripeCustomerId,
        plan: planId
    }, function (err, subscription) {
        cb(null, subscription);
    });
}

/**
 * Associates a stripe customer to a plan id.
 * @param {String} stripeCustomerId stripe customer id.
 * @param {function} cb callback function with err and stripe subscription object
 */
stripeLogic.prototype.addCompanyToRecruiterPlan = function (stripeCustomerId, cb) {
    // Check for user before proceeding.
    if (!stripeCustomerId) {
        return cb('no stripeCustomerId provided to edit.', null);
    }

    this.associatePlan(stripeCustomerId, config.stripe.plan.recruiter, function (err, subscription) {
        return cb(err, subscription);
    });
}

/**
 * Associates a stripe customer to a plan id.
 * @param {String} stripeCustomerId stripe customer id.
 * @param {function} cb callback function with err and stripe subscription object
 */
stripeLogic.prototype.upgradeUserToPremium = function (stripeCustomerId, cb) {
    // Check for user before proceeding.
    if (!stripeCustomerId) {
        return cb('no stripeCustomerId provided to edit.', null);
    }

    this.associatePlan(stripeCustomerId, config.stripe.plan.premiumUser, function (err, subscription) {
        return cb(err, subscription);
    });
}

/**
 * Creates a token from card information.
 * @param {Object} cardInfo information associated with a card.
 * @param {function} cb callback function with err and stripe card token object
 */
stripeLogic.prototype.createCardToken = function (cardInfo, cb) {

    if (!cardInfo) {
        return cb('no card provided to create a token for.', null);
    }

    stripe.tokens.create({
        card: {
            "number": cardInfo.number,
            "exp_month": cardInfo.exp.month,
            "exp_year": cardInfo.exp.year,
            "cvc": cardInfo.cvc
        }
    }, function (err, token) {
        if (err) {
            cb(err, null);
        } else {
            cb(null, token);
        }
    });
}

/**
 * Associates a card to the user, but does not make it the default card.
 * @param {String} stripeCustomerId stripe customer id.
 * @param {String} cardTokenId card token id.
 * @param {function} cb callback function with err and stripe card token object
 */
stripeLogic.prototype.associateCardToUser = function (stripeCustomerId, cardTokenId, cb) {
    // Check for user before proceeding.
    if (!stripeCustomerId) {
        return cb('no stripeCustomerId provided.', null);
    }

    if (!cardTokenId) {
        return cb('no cardTokenId provided', mull);
    }

    stripe.customers.createSource(
        stripeCustomerId, {
            source: cardTokenId
        },
        function (err, card) {
            if (err) {
                cb(err, null);
            } else {
                cb(null, card);
            }
        }
    );
}

module.exports = stripeLogic;