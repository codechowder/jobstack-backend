/**
 * @description Checks if every element in items array is in possible array.
 * @param {array} items the array to search.
 * @param {array} possible the array providing items to check for in the haystack.
 * @return {boolean} true|false if haystack contains at least one item from arr.
 */
var arrayContainsAll = function (items, possible) {
    var isSuperSet = possible.every((v) => {
        return items.indexOf(v) >= 0;
    });

    return isSuperSet;
};

module.exports = arrayContainsAll;