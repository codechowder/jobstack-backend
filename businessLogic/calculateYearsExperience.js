var Account = require("../models/accounts").model;

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();

    return months;
}

/**
 * Gets the years of experience from the jobHistory of an account
 * @param {Account} account the account to calculate the years experience 
 * @param {String} skillId the Skill Id to check against 
 */
var calculateYearsExperience = function (account, skillId) {

    if (!account && !account.jobHistory && !account.skills) {
        return 0;
    }

    //Get the dates
    var dates = [];
    account.jobHistory.forEach((job) => {
        var index = -1;
        if (job.skills && job.skills.length > 0) {
            index = job.skills.findIndex((s) => {
                return skillId === -1 || s.toString() === skillId;
            });
        }
        if (index >= 0 || skillId === -1 && job.fromDate && Date.parse(job.fromDate)) {
            var date = {
                start: new Date(job.fromDate),
                end: !job.toDate ? new Date() : new Date(job.toDate)
            };
            dates.push(date);
        }
    });

    if (dates.length === 0) {
        return 0;
    }

    dates = dates.sort((a, b) => {
        return a.start.getTime() - b.start.getTime();
    })

    //Get min and max date from dates
    var min = dates.reduce((a, b) => {
        return a.start < b.start ? a : b;
    });

    var max = dates.reduce((a, b) => {
        return a.end > b.end ? a : b;
    });

    var months = monthDiff(min.start, max.end);

    //Setup boolean array for months worked
    var boolArr = [];

    for (var i = 0; i < months; i++) {
        boolArr.push(0);
    }

    dates.forEach((element, index, array) => {
        var shiftRightMonth = monthDiff(min.start, element.start);
        var totalMonths = monthDiff(element.start, element.end);
        for (var i = shiftRightMonth; i < shiftRightMonth + totalMonths; i++) {
            boolArr[i] = 1;
        }
    });

    var totalYearsOfExperMonths = 0;
    boolArr.forEach((boolObj, index, array) => {
        totalYearsOfExperMonths += boolObj;
    });

    var totalYearOfExp = totalYearsOfExperMonths / 12;

    return totalYearOfExp;
}

module.exports = calculateYearsExperience;