let JwtStrategy = require('passport-jwt').Strategy;
let FacebookStrategy = require('passport-facebook').Strategy;
let ExtractJwt = require('passport-jwt').ExtractJwt;
let Account = require('../models/accounts').model;
let JobHistory = require("../models/jobhistory").model;
let config = require('../config/');
let FB = require('fb');
let TwitterStrategy = require('passport-twitter').Strategy;
let LinkedInStrategy = require('passport-linkedin').Strategy;

// Setup work and export for the JWT passport strategy
module.exports = function (passport) {
  let opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeader(),
    secretOrKey: config.auth.secret
  };

  passport.serializeUser(Account.serializeUser());
  passport.deserializeUser(Account.deserializeUser());

  passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
    Account.findOne({
      _id: jwt_payload.id
    }, (err, user) => {
      if (err) {
        return done(err, false);
      }
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    });
  }));

  passport.use(new FacebookStrategy({
    clientID: config.facebook.clientId,
    clientSecret: config.facebook.appSecret,
    callbackURL: config.facebook.callback
  }, (accessToken, refreshToken, profile, cb) => {
    Account.findOne({
      facebookId: profile.id
    }, (err, user) => {
      if (err) {
        return cb(err, false);
      }
      if (user) {
        cb(null, user);
      } else {
        FB.setAccessToken(accessToken);
        //Create the user
        FB.api("/me", {
          fields: ['id', 'name', 'first_name', 'last_name', 'email', 'picture', 'work']
        }, function (response) {
          if (response && !response.error) {
            let newAccount = new Account({
              facebookId: response.id,
              firstName: response.first_name,
              lastName: response.last_name,
              //TODO:  Do we need to save this to Sf3 or something?
              profilePic: response.picture.data.url,
              email: response.email,
              role: "user"
            });

            response.work.forEach( (w) => {
              var jobHistory = new JobHistory({
                  companyName: w.employer.name,
                  fromDate: w.start_date,
                  toDate: w.end_date,
                  isCurrent: !w.end_date,
                  position: w.position ? w.position.name : '',
                  skills: []
                });
              
              newAccount.jobHistory.push(jobHistory);

            });

            cb(null, newAccount);
          }
        })


      }
    });
  }));

  passport.use(new TwitterStrategy({
    consumerKey: config.twitter.consumerKey,
    consumerSecret: config.twitter.consumerSecret,
    callbackURL: config.twitter.callbackUrl
  }, (token, tokenSecret, profile, cb) => {
    Account.findOne({
      twitterId: profile.id
    }, (err, user) => {
      if (err) {
        return cb(err, false);
      }
      if (user) {
        cb(null, user);
      } else {
        //Create the user
        let newAccount = new Account({
          twitterId: profile.id,
          //TODO:  Do we need to save this to S3 or something?
          //TODO:  This needs to be fixed before we deploy
          email: response.email !== null ? response.email : "abc@123.com",
          role: "user"
        });

        if (profile.photos && profile.photos.length > 0) {
          newAccount.profilePic = profile.photos[0].value;
        }

        cb(null, newAccount);
      }
    });
  }));

  //TODO: In order for us to get education, past work experience, skills, certs, etc we need to
  //have a partnership program with LinkedIn - https://www.linkedin.com/help/linkedin/ask/API-DVR?lang=en
  passport.use(new LinkedInStrategy({
    consumerKey: config.linkedin.consumerKey,
    consumerSecret: config.linkedin.consumerSecret,
    callbackURL: config.linkedin.callbackUrl,
    profileFields: ['id','first-name','email-address','last-name','picture-url',
    'positions:(id,title,summary,start-date,end-date,is-current,company:(id,name))',
    'educations:(id,school-name,field-of-study,start-date,end-date,degree)',
    'languages:(id,language:(name),proficiency:(level,name))',
    'skills:(id,skill:(name))',
    'certifications:(id,name,authority:(name),number,start-date,end-date)']
  }, (token, tokenSecret, profile, cb) => {
    Account.findOne({
      linkedInId: profile.id
    }, (err, user) => {
      if (err) {
        return cb(err, false);
      }
      if (user) {
        cb(null, user);
      } else {
        //Create the user
        let newAccount = new Account({
          linkedInId: profile.id,
          profilePic: profile.pictureUrl,
          firstName: profile.firstName,
          lastName: profile.lastName,
          //TODO:  Do we need to save this to S3 or something?
          //TODO:  This needs to be fixed before we deploy
          email: response.emailAddress !== null ? response.emailAddress : "abc@123.com",
          role: "user"
        });

        response.position.forEach( (w) => {
              var jobHistory = new JobHistory({
                  companyName: w.company.name,
                  fromDate: new Date(w.startDate.year, w.startDate.month),
                  toDate: w.end_date,
                  isCurrent: w.isCurrent,
                  description: w.summary,
                  position: w.title,
                  skills: []
                });
              
              newAccount.jobHistory.push(jobHistory);
        });

        cb(null, newAccount);
      }
    });
  }));
};